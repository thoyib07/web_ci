/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.21-MariaDB : Database - db_apl_tks
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `m_peserta` */

DROP TABLE IF EXISTS `m_peserta`;

CREATE TABLE `m_peserta` (
  `id_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peserta` varchar(255) DEFAULT NULL,
  `jk` char(1) DEFAULT NULL COMMENT 'L = ''Laki-laki''; P = ''Perempuan''',
  `alamat` text,
  `kelurahan_id` int(11) DEFAULT NULL,
  `kecamatan_id` int(11) DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `provinsi_id` int(11) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `telp` varchar(25) DEFAULT NULL,
  `cdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1 = ''Aktif''; 0 = ''Tidak Aktif''',
  PRIMARY KEY (`id_peserta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `m_status_tks` */

DROP TABLE IF EXISTS `m_status_tks`;

CREATE TABLE `m_status_tks` (
  `id_status_tks` int(11) NOT NULL AUTO_INCREMENT,
  `status_tks` varchar(500) DEFAULT NULL,
  `cdd` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_status_tks`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `m_user` */

DROP TABLE IF EXISTS `m_user`;

CREATE TABLE `m_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_lvl` tinyint(3) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1 = ''Aktif''; 0 = ''Tidak Aktif''',
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `m_user_lvl` */

DROP TABLE IF EXISTS `m_user_lvl`;

CREATE TABLE `m_user_lvl` (
  `id_user_lvl` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(500) DEFAULT NULL,
  `cdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1 = ''Aktif''; 0 = ''Tidak Aktif''',
  PRIMARY KEY (`id_user_lvl`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `t_berita` */

DROP TABLE IF EXISTS `t_berita`;

CREATE TABLE `t_berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(500) DEFAULT NULL,
  `gambar` text,
  `isi` text,
  `publish` tinyint(1) DEFAULT '0' COMMENT '0 = Editor; 1 = Publish',
  `cdd` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 = Tidak Aktif; 1 = Aktif',
  PRIMARY KEY (`id_berita`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `t_berita_link` */

DROP TABLE IF EXISTS `t_berita_link`;

CREATE TABLE `t_berita_link` (
  `id_berita_link` int(11) NOT NULL AUTO_INCREMENT,
  `id_berita` int(11) DEFAULT NULL,
  `link` text,
  `cdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 = Tidak Aktif; 1 = Aktif',
  PRIMARY KEY (`id_berita_link`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `t_ebook` */

DROP TABLE IF EXISTS `t_ebook`;

CREATE TABLE `t_ebook` (
  `id_ebook` int(11) NOT NULL AUTO_INCREMENT,
  `judul_ebook` varchar(500) DEFAULT NULL,
  `ebook` text,
  `cdd` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 = Tidak Aktif; 1 = Aktif',
  PRIMARY KEY (`id_ebook`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `t_gambar_album` */

DROP TABLE IF EXISTS `t_gambar_album`;

CREATE TABLE `t_gambar_album` (
  `id_gambar_album` int(11) NOT NULL AUTO_INCREMENT,
  `judul_album` text,
  `cdd` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 = Tidak Aktif; 1 = Aktif',
  PRIMARY KEY (`id_gambar_album`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `t_gambar_detail` */

DROP TABLE IF EXISTS `t_gambar_detail`;

CREATE TABLE `t_gambar_detail` (
  `id_gambar_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_gambar_album` int(11) DEFAULT NULL,
  `gambar` text,
  `cdd` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 = Tidak Aktif; 1 = Aktif',
  PRIMARY KEY (`id_gambar_detail`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `t_peserta_tks` */

DROP TABLE IF EXISTS `t_peserta_tks`;

CREATE TABLE `t_peserta_tks` (
  `id_peserta_tks` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(550) DEFAULT NULL,
  `jk` tinyint(1) DEFAULT '1' COMMENT '1 : Laki-laki; 0 : Perempuan',
  `id_vilage_alamat` varchar(11) DEFAULT NULL,
  `alamat` text,
  `tgl_lahir` date DEFAULT NULL,
  `id_tempat_lahir` int(11) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `status_pendidikan` text,
  `id_vilage_lokasi_tugas` varchar(11) DEFAULT NULL,
  `alamat_lokasi_tugas` text,
  `id_status_tks` int(11) DEFAULT NULL,
  `cdd` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1 : Aktif; 0 : Tidak Aktif',
  PRIMARY KEY (`id_peserta_tks`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
