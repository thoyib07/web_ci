<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index() {
		if(!isset($_SESSION)) { session_start(); }

		active_time();
		if ($this->session->userdata('login_admin') == 1) {
			//var_dump('masuk');
			redirect(base_url().'admin/home');
		} else {
			redirect(base_url().'auth/login');
		}
	}

	public function home() {
		active_time();
		$data['page'] 	= array('p' => 'home', 'c' => '' );
		$data['title'] 	= ADMIN_TITLE;

		$data['user'] = info_user();
		
		// $data = array_merge($data,side_menu());

		// var_dump("<hr>",$data);
		// die();
		$data['content'] 	= $this->load->view('home',$data,TRUE);
		$this->load->view('layout',$data);
	}

	public function berita($action=null,$id=null) {
		active_time();
		if ((isSuper()) || (isOperator())) { } else { redirect(base_url('admin')); } // Jika bukan super admin atau operator maka dilempar ke halaman home admin
		$data['page'] 	= array('p' => 'berita', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - Berita';
		$data['user'] = info_user();
		//menu		
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/berita');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'tambah':
				$data['action'] = base_url('admin/berita/tambah');

				$jenis_berita['select'] 	= "*";
				$jenis_berita['from']		= "m_jenis_berita";
				$jenis_berita['where']		= "status = 1";
				$data['jenis_berita']		= $this->m_admin->getData($jenis_berita);

				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
					$name = "gambar";
					$new_name = time();
					if ($_FILES[$name]['error'] !== 1) {		
						$type = "jpg|png|jpeg|pdf";
						switch ($_FILES[$name]['type']) {
						case "image/jpeg" :
							$path = "images";
							$real_name = str_replace([".jpg",".jpeg",".JPG",".JPEG"],"",$_FILES[$name]['name']);
							$real_name = str_replace([" ","."],"_",$real_name);
							$nama_file = $real_name."-".$new_name.'.jpeg';
							break;
						case "image/png" :
							$path = "images";
							$real_name = str_replace([".png",".PNG"],"",$_FILES[$name]['name']);
							$real_name = str_replace([" ","."],"_",$real_name);
							$nama_file = $real_name."-".$new_name.'.png';
							break;
						default:
							$path = "dump";
							$real_name = str_replace([" "],"_",$_FILES[$name]['name']);
							$nama_file = $real_name;
							break;
						}
						
						$gambar = unggah_berkas($path,$nama_file,$type);
						$this->upload->do_upload($name);
					}

					$data['data']['judul'] = $_POST['judul'];
					$data['data']['id_jenis_berita'] = $_POST['id_jenis_berita'];
					$data['data']['isi'] = $_POST['isi'];
					$data['data']['gambar']  = "assets/uploads/".$path."/".$gambar;

					if (isSuper()) {
						$data['data']['publish'] = 1;
					} else {
						$data['data']['publish'] = 0;
					}
					
					$data['data']['cdd'] = date('Y-m-d H:i:s');
					$data['data']['status'] = 1;
					// var_dump($data['data']);
					// die();
					$data['table']		= "t_berita";
  					$this->m_admin->addData($data);
					redirect('admin/berita');
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'berita', 'c' => 'tambah berita' );
					$data['content'] 	= $this->load->view('berita/form_berita',$data,TRUE);
				}
			break;

			case 'ubah':
				$data['action'] = base_url('admin/berita/ubah');
				$berita_detail['select']	= "b.*";
				$berita_detail['table']	= "t_berita as b";
				
				if (isset($id)) {
					$berita_detail['where']	= "b.id_berita = '".$id."' and b.status = 1";
				} else {
					$berita_detail['where']	= "b.id_berita = '".$_POST['id_berita']."' and b.status = 1";
				}
				
				$data['berita_detail'] 	= $this->m_admin->getData($berita_detail);

				$jenis_berita['select'] 	= "*";
				$jenis_berita['from']		= "m_jenis_berita";
				$jenis_berita['where']		= "status = 1";
				$data['jenis_berita']		= $this->m_admin->getData($jenis_berita);
					
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// var_dump($data['berita_detail'],"<hr>");
					// die();
					$gambar = "";
					$name = "gambar";
					if ($_FILES[$name]['name'] !== "") {
						// Gambar baru
						$new_name = time();
						if ($_FILES[$name]['error'] !== 1) {		
							$type = "jpg|png|jpeg|pdf";
							switch ($_FILES[$name]['type']) {
							case "image/jpeg" :
								$path = "images";
								$real_name = str_replace([".jpg",".jpeg",".JPG",".JPEG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.jpeg';
								break;
							case "image/png" :
								$path = "images";
								$real_name = str_replace([".png",".PNG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.png';
								break;
							default:
								$path = "dump";
								$real_name = str_replace([" "],"_",$_FILES[$name]['name']);
								$nama_file = $real_name;
								break;
							}
							
							$gambar = unggah_berkas($path,$nama_file,$type);
							$this->upload->do_upload($name);
						}
					}

					$data['data']['judul'] = $_POST['judul'];
					$data['data']['id_jenis_berita'] = $_POST['id_jenis_berita'];
					$data['data']['isi'] = $_POST['isi'];
					
					if ($gambar !== "") {
						$data['data']['gambar']  = "assets/uploads/".$path."/".$gambar;
					} else {
						$data['data']['gambar']  = $data['berita_detail'][0]['gambar'];
					}
					
					if (isSuper()) {
						if ($data['berita_detail'][0]['publish'] == 1) {
							$data['data']['publish'] = $data['berita_detail'][0]['publish'];
						} else {
							$data['data']['publish'] = 1;
						}
						
					} else {
						$data['data']['publish'] = $data['berita_detail'][0]['publish'];
					}
					
					
					$data['data']['cdd'] = $data['berita_detail'][0]['cdd'];
					$data['data']['status'] = $data['berita_detail'][0]['status'];

					$data['table']		= "t_berita";
					$data['where'][0] 	= array('id_berita', $_POST['id_berita']);
  					$this->m_admin->updateData($data); 
  					redirect('admin/berita');
				} else {
					//var_dump($data['berita_detail']);
					$data['page'] 	= array('p' => 'berita', 'c' => 'ubah berita' );
					$data['content'] 	= $this->load->view('berita/form_berita',$data,TRUE);
				}
			break;

			case 'hapus':
				$data['search']	= "id_berita";
				$data['id']		= $id;
				$data['table']	= "t_berita";

				$this->m_admin->delById($data);
				redirect('admin/berita');			
			break;

			case 'restore':
				$data['search']	= "id_berita";
				$data['id']		= $id;
				$data['table']	= "t_berita";

				$this->m_admin->restorById($data);
				redirect('admin/berita');
			break;
			
			default:
				$data['page'] 		= array('p' => 'berita', 'c' => '' );

				$data['content'] 	= $this->load->view('berita',$data,TRUE);
				// echo $data['content'];
			break;
		}
		
		$this->load->view('layout',$data);
	}

	public function ebook($action=null,$id=null) {
		active_time();
		if ((isSuper()) || (isOperator())) { } else { redirect(base_url('admin')); } // Jika bukan super admin atau operator maka dilempar ke halaman home admin
		$data['page'] 	= array('p' => 'ebook', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - Ebook';

		$data['user'] = info_user();
		//menu		
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/ebook');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'tambah':
				$data['action'] = base_url('admin/ebook/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// die();					
					$ebook = "";
					$name = "ebook";
					if ($_FILES[$name]['name'] !== "") {
						// Ebook baru
						$new_name = time();
						if ($_FILES[$name]['error'] !== 1) {		
							$type = "doc|docx|xls|xlsx|ppt|pptx|pdf";
							switch ($_FILES[$name]['type']) {
							case "application/msword" :  // doc
								$path = "files";
								$real_name = str_replace([".doc",".DOC"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.doc';
								break;
							case "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : // docx
								$path = "files";
								$real_name = str_replace([".docx",".DOCX"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.docx';
								break;
							case "application/vnd.ms-excel" : // xls
								$path = "files";
								$real_name = str_replace([".xls",".XLS"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.xls';
								break;
							case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" : // xlsx
								$path = "files";
								$real_name = str_replace([".xlsx",".XLSX"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.xlsx';
								break;
							case "application/vnd.ms-powerpoint" : // ppt
								$path = "files";
								$real_name = str_replace([".ppt",".PPT"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.ppt';
								break;
							case "application/vnd.openxmlformats-officedocument.presentationml.presentation" : // pptx
								$path = "files";
								$real_name = str_replace([".pptx",".PPTX"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.pptx';
								break;
							case "application/pdf" : //pdf
								$path = "files";
								$real_name = str_replace([".pdf",".PDF"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.pdf';
								break;
							default:
								$path = "dump";
								$real_name = str_replace([" "],"_",$_FILES[$name]['name']);
								$nama_file = $real_name;
								break;
							}
							
							$ebook = unggah_berkas($path,$nama_file,$type);
							$this->upload->do_upload($name);
						}
					}
					$real_path_ebook  = "assets/uploads/".$path."/".$ebook;
					$data['data'] 		= array('judul_ebook' => $_POST['judul_ebook'], 'ebook' => $real_path_ebook, 'cdd' => date('Y-m-d H:i:s'), 'status' => 1);
  					$data['table']		= "t_ebook";
  					$id_ebook = $this->m_admin->addData($data);
  					redirect('admin/ebook');
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'ebook', 'c' => 'tambah ebook' );
					$data['content'] 	= $this->load->view('ebook/form_ebook',$data,TRUE);
				}
			break;

			case 'ubah':
				$data['action'] = base_url('admin/ebook/ubah');
				$ebook_detail['select']	= "e.*";
				$ebook_detail['table']	= "t_ebook as e";
				$ebook_detail['where']	= "e.id_ebook = '".$id."' and e.status = 1";				
				
				if (isset($id)) {
					$ebook_detail['where']	= "e.id_ebook = '".$id."' and e.status = 1";
				} else {
					$ebook_detail['where']	= "e.id_ebook = '".$_POST['id_ebook']."' and e.status = 1";
				}

				$data['ebook_detail'] 	= $this->m_admin->getData($ebook_detail);
					
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
					$ebook = "";
					$name = "ebook";
					if ($_FILES[$name]['name'] !== "") {
						// Gambar baru
						$new_name = time();
						if ($_FILES[$name]['error'] !== 1) {		
							$type = "doc|docx|xls|xlsx|ppt|pptx|pdf";
							switch ($_FILES[$name]['type']) {
							case "application/msword" :  // doc
								$path = "files";
								$real_name = str_replace([".doc",".DOC"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.doc';
								break;
							case "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : // docx
								$path = "files";
								$real_name = str_replace([".docx",".DOCX"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.docx';
								break;
							case "application/vnd.ms-excel" : // xls
								$path = "files";
								$real_name = str_replace([".xls",".XLS"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.xls';
								break;
							case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" : // xlsx
								$path = "files";
								$real_name = str_replace([".xlsx",".XLSX"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.xlsx';
								break;
							case "application/vnd.ms-powerpoint" : // ppt
								$path = "files";
								$real_name = str_replace([".ppt",".PPT"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.ppt';
								break;
							case "application/vnd.openxmlformats-officedocument.presentationml.presentation" : // pptx
								$path = "files";
								$real_name = str_replace([".pptx",".PPTX"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.pptx';
								break;
							case "application/pdf" : //pdf
								$path = "files";
								$real_name = str_replace([".pdf",".PDF"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.pdf';
								break;
							default:
								$path = "dump";
								$real_name = str_replace([" "],"_",$_FILES[$name]['name']);
								$nama_file = $real_name;
								break;
							}
							
							$ebook = unggah_berkas($path,$nama_file,$type);
							$this->upload->do_upload($name);
						}
					}

					$data['data']['id_ebook'] = $_POST['id_ebook'];
					$data['data']['judul_ebook'] = $_POST['judul_ebook'];
					if ($ebook !== "") {
						$data['data']['ebook']  = "assets/uploads/".$path."/".$ebook;
					} else {
						$data['data']['ebook']  = $data['ebook_detail'][0]['ebook'];
					}
					// var_dump($data['data'],"<hr>");
					// die();
  					$data['table']		= "t_ebook";
					$data['where'][0] 	= array('id_ebook', $_POST['id_ebook']);
  					$this->m_admin->updateData($data);
					redirect('admin/ebook');					
				} else {
					//var_dump($data['user']);
					$data['page'] 	= array('p' => 'ebook', 'c' => 'ubah ebook' );
					$data['content'] 	= $this->load->view('ebook/form_ebook',$data,TRUE);
				}
			break;

			case 'hapus':
				$data['search']	= "id_ebook";
				$data['id']		= $id;
				$data['table']	= "t_ebook";

				$this->m_admin->delById($data);
				redirect('admin/ebook');			
			break;

			case 'restore':
				$data['search']	= "id_ebook";
				$data['id']		= $id;
				$data['table']	= "t_ebook";

				$this->m_admin->restorById($data);
				redirect('admin/ebook');
			break;
			
			default:
				$data['title'] 		= ADMIN_TITLE.' - Ebook';

				$data['page'] 		= array('p' => 'ebook', 'c' => '' );

				$data['content'] 	= $this->load->view('ebook',$data,TRUE);
				// echo $data['content'];
			break;
		}
		
		$this->load->view('layout',$data);
	}

	public function galeri($action=null,$id_gambar_album=null,$id_gambar_detail=null) {
		active_time();
		if ((isSuper()) || (isOperator())) { } else { redirect(base_url('admin')); } // Jika bukan super admin atau operator maka dilempar ke halaman home admin
		$data['page'] 	= array('p' => 'galeri', 'c' => '' );
		$data['title'] 	= ADMIN_TITLE.' - Galeri';
		$data['user'] 	= info_user();
		//menu		
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/galeri');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'tambah':
				$data['action'] = base_url('admin/galeri/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
					$data['data'] 		= array('judul_album' => $_POST['judul_album'], 'cdd' => date('Y-m-d H:i:s'), 'status' => 1);
  					$data['table']		= "t_gambar_album";
  					$id_gambar_album = $this->m_admin->addData($data);

  					$name = "gambar";
  					$c_file = count($_FILES[$name]['name']);
					$images_arr = $_FILES[$name];
  					for ($i=0; $i < $c_file ; $i++) { 
						$new_name = time();

						$_FILES[$name]['name'] = $images_arr['name'][$i];
						$_FILES[$name]['type'] = $images_arr['type'][$i];
						$_FILES[$name]['tmp_name'] = $images_arr['tmp_name'][$i];
						$_FILES[$name]['error'] = $images_arr['error'][$i];
						$_FILES[$name]['size'] = $images_arr['size'][$i];

						if ($_FILES[$name]['error'] !== 1) {		
							$type = "jpg|png|jpeg|pdf";
							switch ($_FILES[$name]['type']) {
							case "image/jpeg" :
								$path = "images";
								$real_name = str_replace([".jpg",".jpeg",".JPG",".JPEG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.jpeg';
								break;
							case "image/png" :
								$path = "images";
								$real_name = str_replace([".png",".PNG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.png';
								break;
							default:
								$path = "dump";
								$real_name = str_replace([" "],"_",$_FILES[$name]['name']);
								$nama_file = $real_name;
								break;
							}
							
							$gambar = unggah_berkas($path,$nama_file,$type);
							$this->upload->do_upload($name);
						}
						$real_path_gambar  = "assets/uploads/".$path."/".$gambar;
						// var_dump($real_path_gambar);
						// die();
						
						$data['data'] 		= array('id_gambar_album' => $id_gambar_album, 'gambar' => $real_path_gambar, 'cdd' => date('Y-m-d H:i:s'), 'status' => 1);
	  					$data['table']		= "t_gambar_detail";
	  					$id_gambar_detail = $this->m_admin->addData($data);
  					}
					redirect('admin/galeri');
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'galeri', 'c' => 'tambah galeri' );
					$data['content'] 	= $this->load->view('galeri/form_galeri',$data,TRUE);
				}
			break;

			case 'tambah_foto':
				$data['action'] = base_url('admin/galeri/tambah_foto');
				$data['proses'] = ucwords("Tambah Foto");
				$judul_album['select'] 	= "*";
				$judul_album['from'] 	= "t_gambar_album";
				$judul_album['where'] 	= "status = 1";
				$data['judul_album']	= $this->m_admin->getData($judul_album);

				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
  					$id_gambar_album = $_POST['judul_album'];

  					$name = "gambar";
  					$c_file = count($_FILES[$name]['name']);
					$images_arr = $_FILES[$name];
  					for ($i=0; $i < $c_file ; $i++) { 
						$new_name = time();

						$_FILES[$name]['name'] = $images_arr['name'][$i];
						$_FILES[$name]['type'] = $images_arr['type'][$i];
						$_FILES[$name]['tmp_name'] = $images_arr['tmp_name'][$i];
						$_FILES[$name]['error'] = $images_arr['error'][$i];
						$_FILES[$name]['size'] = $images_arr['size'][$i];

						if ($_FILES[$name]['error'] !== 1) {		
							$type = "jpg|png|jpeg|pdf";
							switch ($_FILES[$name]['type']) {
							case "image/jpeg" :
								$path = "images";
								$real_name = str_replace([".jpg",".jpeg",".JPG",".JPEG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.jpeg';
								break;
							case "image/png" :
								$path = "images";
								$real_name = str_replace([".png",".PNG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.png';
								break;
							default:
								$path = "dump";
								$real_name = str_replace([" "],"_",$_FILES[$name]['name']);
								$nama_file = $real_name;
								break;
							}
							
							$gambar = unggah_berkas($path,$nama_file,$type);
							$this->upload->do_upload($name);
						}
						$real_path_gambar  = "assets/uploads/".$path."/".$gambar;
						// var_dump($real_path_gambar);
						// die();
						
						$data['data'] 		= array('id_gambar_album' => $id_gambar_album, 'gambar' => $real_path_gambar, 'cdd' => date('Y-m-d H:i:s'), 'status' => 1);
	  					$data['table']		= "t_gambar_detail";
	  					$id_gambar_detail = $this->m_admin->addData($data);
  					}
					redirect('admin/galeri');
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'galeri', 'c' => 'tambah foto' );
					$data['content'] 	= $this->load->view('galeri/form_foto',$data,TRUE);
				}
			break;

			case 'ubah':
				$data['action'] = base_url('admin/galeri/ubah');
				$galeri['select']	= "a.*, d.id_gambar_detail, d.gambar";
				$galeri['table']	= "t_gambar_album as a";
				$galeri['join'][0]	= array('t_gambar_detail as d','a.id_gambar_album = d.id_gambar_album' );

				if (isset($id_gambar_detail)) {
					$galeri['where']	= "d.id_gambar_detail = '".$id_gambar_detail."' and d.status = 1 and a.status = 1";
				} else {
					$galeri['where']	= "d.id_gambar_detail = '".$_POST['id_gambar_detail']."' and d.status = 1 and a.status = 1";
				}
				
				$data['galeri_detail'] 	= $this->m_admin->getData($galeri);

				$album['select'] 	= "*";
				$album['table'] 	= "t_gambar_album";
				$album['where']		= "status = 1";
				$data['album']		= $this->m_admin->getData($album);
				$data['select_album']	= '<select id="id_gambar_album" name="id_gambar_album" class="form-control">';
				foreach ($data['album'] as $value) {
					if ($value['id_gambar_album'] == $id_gambar_album) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					
					$data['select_album'] .= '<option value="'.$value['id_gambar_album'].'" '.$selected.'>'.$value['judul_album'].'</option>';
				}
				$data['select_album']	.= '</select>';
				// var_dump($data['galeri_detail']);
				// die();
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
					$gambar = "";
					$name = "gambar";
					if ($_FILES[$name]['name'] !== "") {
						// Gambar baru
						$new_name = time();
						if ($_FILES[$name]['error'] !== 1) {		
							$type = "jpg|png|jpeg";
							switch ($_FILES[$name]['type']) {
							case "image/jpeg" :
								$path = "images";
								$real_name = str_replace([".jpg",".jpeg",".JPG",".JPEG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.jpeg';
								break;
							case "image/png" :
								$path = "images";
								$real_name = str_replace([".png",".PNG"],"",$_FILES[$name]['name']);
								$real_name = str_replace([" ","."],"_",$real_name);
								$nama_file = $real_name."-".$new_name.'.png';
								break;
							default:
								$path = "dump";
								$real_name = str_replace([" "],"_",$_FILES[$name]['name']);
								$nama_file = $real_name;
								break;
							}
							
							$gambar = unggah_berkas($path,$nama_file,$type);
							$this->upload->do_upload($name);
						}
					}
					$data['data']['id_gambar_album'] = $_POST['id_gambar_album'];
					if ($gambar !== "") {
						$data['data']['gambar']  = "assets/uploads/".$path."/".$gambar;
					} else {
						$data['data']['gambar']  = $data['galeri_detail'][0]['gambar'];
					}
					// var_dump($data['data'],"<hr>");
					// die();
  					$data['table']		= "t_gambar_detail";
					$data['where'][0] 	= array('id_gambar_detail', $_POST['id_gambar_detail']);
  					$this->m_admin->updateData($data);
					redirect('admin/galeri');		
				} else {
					//var_dump($data['user']);
					$data['page'] 	= array('p' => 'galeri', 'c' => 'ubah galeri' );
					$data['content'] 	= $this->load->view('galeri/form_galeri',$data,TRUE);
				}
			break;

			case 'hapus':
				$data['search']	= "id_gambar_detail";
				$data['id']		= $id_gambar_detail;
				$data['table']	= "t_gambar_detail";

				$this->m_admin->delById($data);
				redirect('admin/galeri');			
			break;

			case 'restore':
				$data['search']	= "id_gambar_detail";
				$data['id']		= $id_gambar_detail;
				$data['table']	= "t_gambar_detail";

				$this->m_admin->restorById($data);
				redirect('admin/galeri');
			break;
			
			default:
				$data['title'] 		= ADMIN_TITLE.' - Galeri';

				$data['page'] 		= array('p' => 'galeri', 'c' => '' );

				$data['content'] 	= $this->load->view('galeri',$data,TRUE);
				// echo $data['content'];
			break;
		}
		
		$this->load->view('layout',$data);
	}

	public function user($action=null,$id=null) {
		active_time();
		if (!isSuper()) { redirect(base_url('admin')); } // Jika bukan super admin maka dilempar ke halaman home admin
		$data['page'] 	= array('p' => 'user', 'c' => '' );
		$data['title'] 		= ADMIN_TITLE.' - User';
		$data['user'] = info_user();
		//menu		
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/user');
		$data['proses'] = ucwords($action);
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data);
		// die();
		switch ($action) {
			case 'tambah':
				$user_lvl['select']	= "u.*";
				$user_lvl['table']	= "m_user_lvl as u";
				$user_lvl['where']	= "u.status = 1";
				$data['user_lvl'] 	= $this->m_admin->getData($user_lvl);

				$data['action'] = base_url('admin/user/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST);
					// die();
					$data['data'] 		= array('username' => $_POST['username'], 'nama' => $_POST['nama'], 'password' => sha1($_POST['password']), 'id_user_lvl' => $_POST['hak_akses']);
  					$data['table']		= "m_user";
  					$this->m_admin->addData($data);
					redirect('admin/user');
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'user', 'c' => 'tambah user' );
					$data['content'] 	= $this->load->view('user/form_user',$data,TRUE);
				}
			break;

			case 'ubah':
				$user_lvl['select']	= "u.*";
				$user_lvl['table']	= "m_user_lvl as u";
				$user_lvl['where']	= "u.status = 1";
				$data['user_lvl'] 	= $this->m_admin->getData($user_lvl);

				$user['select']	= "u.*";
				$user['table']	= "m_user as u";

				if (isset($id)) {
					$user['where']	= "u.id_user = '".$id."' and u.status = 1";
				} else {
					$user['where']	= "u.id_user = '".$_POST['id_user']."' and u.status = 1";
				}

				$data['user_detail'] 	= $this->m_admin->getData($user);

				$data['action'] = base_url('admin/user/ubah');
					
				if (isset($_POST['submit'])) {
					// var_dump($_POST);
					// die();
					// $data['data'] 		= array('username' => $_POST['username'], 'nama' => $_POST['nama'], 'password' => sha1($_POST['password']), 'id_user_lvl' => $_POST['hak_akses']);
					$data['data'] 		= array('username' => $_POST['username'], 'nama' => $_POST['nama'], 'id_user_lvl' => $_POST['hak_akses']);
  					$data['table']		= "m_user";
					$data['where'][0] 	= array('id_user', $_POST['id_user']);
  					$this->m_admin->updateData($data); 
					// die();
  					redirect('admin/user');
				} else {
					//var_dump($data['user']);
					$data['page'] 	= array('p' => 'user', 'c' => 'ubah user' );
					$data['content'] 	= $this->load->view('user/form_user',$data,TRUE);
				}
			break;

			case 'hapus':
				$data['id']		= $id;
				$data['search']	= "id_user";
				$data['table']	= "m_user";

				$this->m_admin->delById($data);
				redirect('admin/user');			
			break;

			case 'restore':
				$data['id']		= $id;
				$data['search']	= "id_user";
				$data['table']	= "m_user";

				$this->m_admin->restorById($data);
				redirect('admin/user');
			break;
			
			default:
				$data['page'] 		= array('p' => 'user', 'c' => '' );

				$data['content'] 	= $this->load->view('user',$data,TRUE);
				// echo $data['content'];
			break;
		}
		
		$this->load->view('layout',$data);
	}

	public function peserta_tks($action=null,$id=null) {
		active_time();
		if ((isSuper()) || (isOperator())) { } else { redirect(base_url('admin')); } // Jika bukan super admin atau operator maka dilempar ke halaman home admin
		$data['page'] 	= array('p' => 'Peserta TKS', 'c' => '' );
		$data['title'] 	= ADMIN_TITLE;

		$data['user'] = info_user();
		$data['proses'] = ucwords($action);
		//menu		
		// $data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/peserta_tks');
		// $data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'tambah':
				$data['action'] = base_url('admin/peserta_tks/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST);
					// die();
					$id_status_tks = 1;
					if ($_POST['tahun_mulai'] == $_POST['tahun_berakhir']) {
						$id_status_tks = 1;
					} else if ($_POST['tahun_mulai'] <= $_POST['tahun_berakhir']) {
						$id_status_tks = 2;
					}
					
					$data['data'] 		= array(
											'nik' => $_POST['nik'], 
											'nama' => $_POST['nama'], 
											'jk' => $_POST['jk'], 
											'no_hp' => str_replace("_", "", $_POST['no_hp']),
											'id_tempat_lahir' => $_POST['id_tempat_lahir'], 
											// 'tgl_lahir' => date('Y-m-d',strtotime($_POST['tgl_lahir'])),
											'tgl_lahir' => $_POST['tgl_lahir'], 
											'id_vilage_alamat' => $_POST['id_vilage_alamat'], 
											'alamat' => $_POST['alamat'],
											'status_pendidikan' => $_POST['status_pendidikan'], 
											'nama_kelompok_usaha' => $_POST['nama_kelompok_usaha'], 
											'id_vilage_kelompok_usaha' => $_POST['id_vilage_kelompok_usaha'], 
											'alamat_kelompok_usaha' => $_POST['alamat_kelompok_usaha'],
											'deskripsi_kelompok_usaha' => $_POST['deskripsi_kelompok_usaha'], 
											'id_status_tks' => $id_status_tks, 
											'tahun_mulai' => $_POST['tahun_mulai'], 
											'tahun_berakhir' => $_POST['tahun_berakhir'],
											'cdd' => date('Y-m-d H:i:s'), 
											'status' => 1
										);
  					$data['table']		= "m_peserta_tks";
  					$this->m_admin->addData($data);
					redirect('admin/peserta_tks');
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'Peserta TKS', 'c' => 'tambah peserta tks' );
					$data['content'] 	= $this->load->view('peserta_tks/form_peserta_tks',$data,TRUE);
				}
			break;

			case 'ubah':
				$data['action'] = base_url('admin/peserta_tks/ubah');
				$peserta_tks_detail['select']	= "p.*, r_k.id as kab_kelompok_id, r_k.province_id as prov_kelompok_id, r_a.id as kab_alamat_id, r_a.province_id as prov_alamat_id, r_l.id as kab_lahir_id, r_l.province_id as prov_lahir_id";
				$peserta_tks_detail['table']	= "m_peserta_tks as p";
				$peserta_tks_detail['where']	= "p.id_peserta_tks = '".$id."' and p.status = 1";
				$peserta_tks_detail['join'][0]	= array("regencies as r_k","r_k.id = p.id_vilage_kelompok_usaha");
				$peserta_tks_detail['join'][1]	= array("provinces as pr_k","pr_k.id = r_k.province_id");

				$peserta_tks_detail['join'][2]	= array("regencies as r_a","r_a.id = p.id_vilage_alamat");
				$peserta_tks_detail['join'][3]	= array("provinces as pr_a","pr_a.id = r_a.province_id");

				$peserta_tks_detail['join'][4]	= array("regencies as r_l","r_l.id = p.id_tempat_lahir");
				$peserta_tks_detail['join'][5]	= array("provinces as pr_l","pr_l.id = r_l.province_id");
				$data['peserta_tks_detail'] 	= $this->m_admin->getData($peserta_tks_detail);
					
				if (isset($_POST['submit'])) {
					// var_dump($_POST);
					// die();
					$id_status_tks = $_POST['id_status_tks'];
					if ($_POST['tahun_mulai'] == $_POST['tahun_berakhir']) {
						$id_status_tks = 1;
					} else if ($_POST['tahun_mulai'] <= $_POST['tahun_berakhir']) {
						$id_status_tks = 2;
					}
					$data['data'] 		= array(
											'nik' => $_POST['nik'], 
											'nama' => $_POST['nama'], 
											'jk' => $_POST['jk'], 
											'no_hp' => str_replace("_", "", $_POST['no_hp']),
											'id_tempat_lahir' => $_POST['id_tempat_lahir'], 
											// 'tgl_lahir' => date('Y-m-d',strtotime($_POST['tgl_lahir'])),
											'tgl_lahir' => $_POST['tgl_lahir'], 
											'id_vilage_alamat' => $_POST['id_vilage_alamat'], 
											'alamat' => $_POST['alamat'],
											'status_pendidikan' => $_POST['status_pendidikan'], 
											'nama_kelompok_usaha' => $_POST['nama_kelompok_usaha'], 
											'id_vilage_kelompok_usaha' => $_POST['id_vilage_kelompok_usaha'], 
											'alamat_kelompok_usaha' => $_POST['alamat_kelompok_usaha'],
											'deskripsi_kelompok_usaha' => $_POST['deskripsi_kelompok_usaha'], 
											'id_status_tks' => $id_status_tks, 
											'tahun_mulai' => $_POST['tahun_mulai'], 
											'tahun_berakhir' => $_POST['tahun_berakhir'],
											'cdd' => date('Y-m-d H:i:s'), 
											'status' => 1
										);
  					$data['table']		= "m_peserta_tks";
  					$data['where'][0] 	= array('id_peserta_tks', $_POST['id_peserta_tks']);
  					$this->m_admin->updateData($data); 
					redirect('admin/peserta_tks');
				} else {
					// var_dump($data['peserta_tks_detail']);
					$data['page'] 	= array('p' => 'Peserta TKS', 'c' => 'ubah peserta tks' );
					$data['content'] 	= $this->load->view('peserta_tks/form_peserta_tks',$data,TRUE);
				}
			break;

			case 'hapus':
				$data['search']	= "id_peserta_tks";
				$data['id']		= $id;
				$data['table']	= "m_peserta_tks";

				$this->m_admin->delById($data);
				redirect('admin/peserta_tks');			
			break;

			case 'restore':
				$data['search']	= "id_peserta_tks";
				$data['id']		= $id;
				$data['table']	= "m_peserta_tks";

				$this->m_admin->restorById($data);
				redirect('admin/peserta_tks');
			break;
			
			default:
				$data['title'] 		= ADMIN_TITLE.' - Peserta TKS';
				$data['page'] 		= array('p' => 'Peserta TKS', 'c' => '' );
				$data['content'] 	= $this->load->view('peserta_tks',$data,TRUE);
				// echo $data['content'];
			break;
		}
		
		$this->load->view('layout',$data);
	}

	public function ajax_list() {
		$data 	= array();
        $no 	= $_POST['start'];
        $type 	= $_GET['type'];
        if (isset($_GET['id'])) {
        	$id 	= $_GET['id'];
        }
        switch ($type) {
        	case 'user':
        	case 'del_user':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = $l['nama'];
		            $row[] = $l['jabatan'];
		            

		            if ($type == 'user') {
		            	$row[] = '<a href="'.base_url('admin/user/ubah').'/'.$l['id'].'"><button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data User"></button></a>'.''.'<a href="'.base_url('admin/user/hapus').'/'.$l['id'].'" onclick="return confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data User"></button></a>';
		            } else {
		            	 $row[] = '<a href="'.base_url('admin/user/restore').'/'.$l['id'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data User"></button></a>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        		break;

        	case 'berita':
        	case 'del_berita':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = '<img src="'.base_url($l['gambar']).'" width="200px">';
		            $row[] = $l['jenis_berita'];
		            $row[] = $l['judul'];
		            $row[] = $l['cdd'];

		            if ($l['publish'] == '1') {
		            	$row[] = '<h5><span style="color:green">Publis</span></h5>';
		            } else {
		            	$row[] = '<h5><span style="color:blue">Moderasi Admin</span></h5>';
		            }

		            if ($type == 'berita') {
		            	$row[] = '<a href="'.base_url('admin/berita/ubah').'/'.$l['id'].'"><button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Berita"></button></a>'.''.'<a href="'.base_url('admin/berita/hapus').'/'.$l['id'].'" onclick="return confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Berita"></button></a>';
		            } else {
		            	 $row[] = '<a href="'.base_url('admin/berita/restore').'/'.$l['id'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Berita"></button></a>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        		break;

        	case 'galeri':
        	case 'del_galeri':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = '<img src="'.base_url($l['gambar']).'" width="200px">';
		            $row[] = $l['judul_album'];
		            $row[] = $l['cdd'];

		            if ($type == 'galeri') {
		            	$row[] = '<a href="'.base_url('admin/galeri/ubah').'/'.$l['id_gambar_album'].'/'.$l['id_gambar_detail'].'"><button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Galeri"></button></a>'.''.'<a href="'.base_url('admin/galeri/hapus').'/'.$l['id_gambar_album'].'/'.$l['id_gambar_detail'].'" onclick="return confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Galeri"></button></a>';
		            } else {
		            	 $row[] = '<a href="'.base_url('admin/galeri/restore').'/'.$l['id_gambar_album'].'/'.$l['id_gambar_detail'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Galeri"></button></a>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        		break;

        	case 'ebook':
        	case 'del_ebook':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = $l['judul_ebook'];
		            $row[] = $l['cdd'];
		            $file_down = base_url($l['ebook']);
		            if ($type == 'ebook') {
		            	$row[] = '<a href="'.base_url('admin/ebook/ubah').'/'.$l['id_ebook'].'"><button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Ebook"></button></a>'.''.'<a href="'.base_url('admin/ebook/hapus').'/'.$l['id_ebook'].'" onclick="return confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Ebook"></button></a>'.''.'<a href="#" onclick=" return window.open(`'.$file_down.'`, `_blank`)" ><button class="btn btn-primary fa fa-download" type="button" title="Unduh Data Ebook"></button></a>';
		            } else {
		            	 $row[] = '<a href="'.base_url('admin/ebook/restore').'/'.$l['id_ebook'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Ebook"></button></a>'.''.'<a href="#" onclick=" return window.open(`'.$file_down.'`, `_blank`)" ><button class="btn btn-primary fa fa-download" type="button" title="Unduh Data Ebook"></button></a>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        		break;

        	case 'peserta_tks':
        	case 'del_peserta_tks':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = $l['nama'];
		            $row[] = $l['status_pendidikan'];
		            $row[] = $l['nama_kelompok_usaha']." - (".$l['kabupaten_kelompok']." , ".$l['provinsi_kelompok'].")";
		            $row[] = $l['tahun_mulai'];
		            

		            if ($type == 'peserta_tks') {
		            	$row[] = '<a href="'.base_url('admin/peserta_tks/ubah').'/'.$l['id_peserta_tks'].'"><button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Peserta TKS"></button></a>'.''.'<a href="'.base_url('admin/peserta_tks/hapus').'/'.$l['id_peserta_tks'].'" onclick="return confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Peserta TKS"></button></a>';
		            } else {
		            	 $row[] = '<a href="'.base_url('admin/peserta_tks/restore').'/'.$l['id_peserta_tks'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Peserta TKS"></button></a>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        		break;
        	
        	default:
        		# code...
        		break;
        }
        //output to json format
        echo json_encode($output);
	}

	public function search() {
		var_dump(base_url());die();
	}

	public function ajax($type) {
		switch ($type) {
			case 'ajax_provinsi':
				$data = (object) array();
				$provinsi['select'] 	= "*";
				$provinsi['table']		= "provinces";
				$provinsi['order']		= array("name","asc");
		        $data->provinsi = $this->m_admin->getData($provinsi);
				break;

			case 'ajax_kabupaten':
				$province_id	= $_GET['province_id'];
				$data = (object) array();
				$kabupaten['select'] 	= "*";
				$kabupaten['table']		= "regencies";
				$kabupaten['where']		= "province_id = '".$province_id."'";
				$kabupaten['order']		= array("name","asc");
		        $data->kabupaten = $this->m_admin->getData($kabupaten);
				break;
			
			default:
				# code...
				break;
		}
		echo json_encode($data);
	}

	public function api($type) {
		switch ($type) {
			case 'test':
				# code...
				break;
			
			default:
				# code...
				break;
		}
	}
}
?>