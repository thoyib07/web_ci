<?php 
// var_dump($action); 
?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>bower_components/ckeditor/dataTables.bootstrap.css"> -->
<script src="<?php echo base_url('assets/admin/'); ?>bower_components/ckeditor/ckeditor.js"></script>
<div id="area_input_new" style="display:none"></div>
<div id="content">
  <section class="content-header">
    <h1><?php echo $title?> <small> <b>Dashboard</b> Control panel</small> </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('admin/berita') ?>"> Berita</a></li>
        <li class="active"><?php echo ucwords($proses); ?> Berita</a></li>
      </ol>
  </section>
  <section class='content'>
    <div class='row'>    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
            <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <strong><?= $this->session->flashdata('error') ?></strong>
              </div>
            <?php } ?>
              <input type="hidden" name="id_berita" id="id_berita" value="<?php echo @$berita_detail[0]['id_berita'] ?>">
              <div class="form-group">
                <label for="id_jenis_berita">Jenis Berita <font color="red">*</font></label>
                <select id="id_jenis_berita" name="id_jenis_berita" class="form-control" required>
                  <?php foreach ($jenis_berita as $v_jb) { ?>
                    <option value="<?php echo @$v_jb['id_jenis_berita']; ?>" <?php if (@$berita_detail[0]['id_jenis_berita'] == @$v_jb['id_jenis_berita'] ) { echo "selected"; } ?> ><?php echo strtoupper(@$v_jb['jenis_berita']); ?> </option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label for="judul">Judul Berita <font color="red">*</font></label>
                <input class="form-control" id="judul" placeholder="Judul Berita" type="text" name="judul" value="<?php echo @$berita_detail[0]['judul'] ?>" required>
              </div>

              <div class="form-group">
                <label for="gambar">Gambar Cover Berita<?php if (strtolower($proses) !== "ubah") { ?><font color="red">*</font><?php } ?></label>
                <input id="gambar" placeholder="Gambar Cover Berita" type="file" name="gambar" accept="image/x-png,image/jpeg" onchange="hide_img();" <?php if (strtolower($proses) !== "ubah") { echo "required"; }?> >
                <?php if (strtolower($proses) == "ubah") { ?>
                <div id="div_gambar_old">
                  <hr>
                  <font color="red">Gambar Sebelumnya : </font><br>
                  <img src="<?php echo base_url().@$berita_detail[0]['gambar'] ?>" width="200px">
                </div>
                <?php } ?>
              </div>

              <div class="form-group">
                <label for="isi">Isi Berita <font color="red">*</font></label>
                <textarea class="form-control" name="isi" id="isi" required ><?php echo @$berita_detail[0]['isi'] ?></textarea>
              </div>

              <div class="form-group">
                <span id="text_submit"></span>
                <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success">
                <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    // akses();
    // getopendata();
    CKEDITOR.replace( 'isi' );
});

function hide_img() {
  $("#div_gambar_old").hide();
}

$('#submit_btn').on('click',function () {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
});

$( "form" ).submit(function(e) {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
  // if ($('#hak_akses').val() == 1) {
  //   return;
  // } else {
  //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
  //     return;
  //   } else {
  //     alert('Hak akses harus dipilih!!!');
  //     e.preventDefault(e);
  //   }
  // }  
});

</script>