<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo @$title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/ebook'); ?>" ><i class="fa fa-dashboard"></i> <?php echo ucwords($page['p']); ?></a></li>
        <li class="active"><?php echo ucwords($page['c']); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" title="Ebook Aktif">Ebook</a></li>
          <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" title="Ebook Tidak Aktif">Ebook Tidak Aktif</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
              <a href="<?php echo base_url('admin/ebook/tambah'); ?>" ><button class="btn btn-success fa fa-plus"> Tambah Ebook </button></a>
              <table id="ebook" class="table table-bordered text-center" style="width: 100% !important;">
                <thead>
                  <tr>
                    <td>No</td>
                    <td>Judul Ebook</td>
                    <td>Tanggal Buat</td>
                    <td>Perintah</td>
                  </tr>
                </thead>
              </table>
          </div>
                      
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_3">
            <table id="trash_ebook" class="table table-bordered text-center" style="width: 100% !important;">
              <thead>
                  <tr>
                    <td>No</td>
                    <td>Judul Ebook</td>
                    <td>Tanggal Buat</td>
                    <td>Perintah</td>
                  </tr>
                </thead>
            </table>
          </div>

        </div>
      <!-- /.tab-content -->
      </div>
    </section>
    <!-- /.content -->

<script type="text/javascript">

var table;
var table_trash;

$(document).ready(function() {
    
    //datatables
    //function kategori() {
      table = $('#ebook').DataTable({ 
 
        "processing"  : true, //Feature control the processing indicator.
        "serverSide"  : true, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 0.5 * 1000,
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/admin/ajax_list?type=ebook')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 3 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      });
    //}
    
    //function trash_kategori() {
      table_trash = $('#trash_ebook').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 0.5 * 1000,
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/admin/ajax_list?type=del_ebook')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 3 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      });
    //}
    //}
    // Setiap 1 menit perbarui data
    setInterval( function () {
      table.ajax.reload(null,false);
      table_trash.ajax.reload(null,false);
    }, 60 * 1000 );
 
});
</script>