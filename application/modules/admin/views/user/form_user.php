<?php 
// var_dump($action); 
?>
<div id="area_input_new" style="display:none"></div>
<div id="content">
  <section class="content-header">
    <h1><?php echo $title?> <small> <b>Dashboard</b> Control panel</small> </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('admin/user') ?>"> User</a></li>
        <li class="active"><?php echo ucwords($proses); ?> User</a></li>
      </ol>
  </section>
  <section class='content'>
    <div class='row'>    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
            <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <strong><?= $this->session->flashdata('error') ?></strong>
              </div>
            <?php } ?>
              <input type="hidden" name="id_user" id="id_user" value="<?php echo @$user_detail[0]['id_user'] ?>">
              <div class="form-group">
                <label for="nama">Nama <font color="red">*</font></label>
                <input class="form-control" id="nama" placeholder="Nama User" type="text" name="nama" value="<?php echo @$user_detail[0]['nama'] ?>" required>
              </div>

              <div class="form-group">
                <label for="username">Username <font color="red">*</font></label>
                <input class="form-control" id="username" placeholder="Username" type="text" name="username" value="<?php echo @$user_detail[0]['username'] ?>" required>
              </div>

              <?php if (strtolower($proses) !== "ubah") { ?>
              <div class="form-group">
                <label for="password">Passwors <font color="red">*</font></label>
                <input class="form-control" id="password" placeholder="Password" type="password" name="password" value="" required>
              </div>
              <?php } ?>

              <div class="form-group">
                <label for="hak_akses">Hak Akses <font color="red">*</font></label>
                <select class="form-control" id="hak_akses" name="hak_akses" required>
                  <?php foreach ($user_lvl as $lvl) { ?>
                    <option value="<?php echo $lvl['id_user_lvl']; ?>" <?php if (@$user_detail[0]['id_user_lvl'] == $lvl['id_user_lvl']) { echo "selected"; } ?> ><?php echo strtoupper($lvl['role']); ?></option>
                  <? } ?>
                </select>
              </div>

              <div class="form-group">
                <span id="text_submit"></span>
                <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success">
                <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    // akses();
    // getopendata();
});

$('#submit_btn').on('click',function () {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
});

$( "form" ).submit(function(e) {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
  // if ($('#hak_akses').val() == 1) {
  //   return;
  // } else {
  //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
  //     return;
  //   } else {
  //     alert('Hak akses harus dipilih!!!');
  //     e.preventDefault(e);
  //   }
  // }  
});

</script>