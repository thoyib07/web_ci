<?php 
// var_dump($action); 
?>

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bower_components/select2/dist/css/select2.min.css">
<!-- bootstrap datepicker -->
<script src="<?php echo base_url('assets/admin/'); ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/admin/'); ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>

<div id="area_input_new" style="display:none"></div>
<div id="content">
  <section class="content-header">
    <h1><?php echo $title?> <small> <b>Dashboard</b> Control panel</small> </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('admin/peserta_tks') ?>"> Peserta TKS</a></li>
        <li class="active"><?php echo ucwords($proses); ?> Peserta TKS</a></li>
      </ol>
  </section>
  <section class='content'>
    <div class='row'>    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

        <div class="col-md-12">
          <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Data Diri Peserta</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>

            <div class="box-body">
              <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <strong><?= $this->session->flashdata('error') ?></strong>
              </div>
              <?php } ?>
              <input type="hidden" name="id_peserta_tks" id="id_peserta_tks" value="<?php echo @$peserta_tks_detail[0]['id_peserta_tks'] ?>">
              <input type="hidden" name="id_status_tks" id="id_status_tks" value="<?php echo @$peserta_tks_detail[0]['id_status_tks'] ?>">

              <div class="form-group">
                <label for="nik">NIK <font color="red">*</font></label>
                <input class="form-control" id="nik" data-inputmask='"mask": "9999999999999999"' data-mask placeholder="Nomer Induk Kependudukan" type="text" name="nik" value="<?php echo @$peserta_tks_detail[0]['nik'] ?>" required>
              </div>

              <div class="form-group">
                <label for="nama">Nama Peserta TKS<font color="red">*</font></label>
                <input class="form-control" id="nama" placeholder="Nama Peserta TKS" type="text" name="nama" value="<?php echo @$peserta_tks_detail[0]['nama'] ?>" required>
              </div>

              <div class="form-group">
                <label for="jk">Jenis Kelamin <font color="red">*</font></label>
                <select id="jk" name="jk" class="form-control select2">
                  <option value="1" <?php if (@$peserta_tks_detail[0]['jk'] == "1") { echo "selected"; } ?>>Laki-Laki</option>
                  <option value="0" <?php if (@$peserta_tks_detail[0]['jk'] == "0") { echo "selected"; } ?>>Perempuan</option>
                </select>
              </div>

              <div class="form-group">
                <label for="no_hp">No. HP / No. Telp </label>
                <input class="form-control" id="no_hp" data-inputmask='"mask": ["9999999999","999999999999999"]' data-mask placeholder="Nomer HP / Nomer Telp" type="text" name="no_hp" value="<?php echo @$peserta_tks_detail[0]['no_hp'] ?>">
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="province_lahir_id">Provinsi Lahir <font color="red">*</font></label>
                  <select id="province_lahir_id" name="province_lahir_id" class="form-control select2"></select>
                </div>              
              </div>

              <div class="col-md-6">
                <div class="form-group" id="div_id_id_tempat_lahir">
                  <label for="id_tempat_lahir">Kota/Kabupaten Lahir <font color="red">*</font></label>
                  <select id="id_tempat_lahir" name="id_tempat_lahir" class="form-control select2"></select>
                </div>
              </div>

              <div class="form-group">
                <label for="tgl_lahir">Tanggal Lahir <font color="red">*</font></label>
                <input class="form-control datepicker" id="tgl_lahir" placeholder="Tanggal Lahir" type="text" name="tgl_lahir" value="<?php echo @$peserta_tks_detail[0]['tgl_lahir'] ?>" required readonly>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="province_tinggal_id">Provinsi Tinggal <font color="red">*</font></label>
                  <select id="province_tinggal_id" name="province_tinggal_id" class="form-control select2"></select>
                </div>              
              </div>

              <div class="col-md-6">
                <div class="form-group" id="div_id_id_tempat_tinggal">
                  <label for="id_vilage_alamat">Kota/Kabupaten Tinggal <font color="red">*</font></label>
                  <select id="id_vilage_alamat" name="id_vilage_alamat" class="form-control select2"></select>
                </div>
              </div>

              <div class="form-group">
                <label for="alamat">Alamat Tinggal <font color="red">*</font></label>
                <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat Tinggal" style="resize: none;" ><?php echo @$peserta_tks_detail[0]['alamat'] ?></textarea>
              </div>

              <div class="form-group">
                <label for="status_pendidikan">Status Pendidikan </label>
                <textarea id="status_pendidikan" name="status_pendidikan" class="form-control" rows="3" placeholder="Status Pendidikan" style="resize: none;"><?php echo @$peserta_tks_detail[0]['status_pendidikan'] ?></textarea>
              </div>

            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">

            <div class="box-header with-border">
              <h3 class="box-title">Data Kelompok Usaha</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>

            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tahun_mulai">Tahun Mulai Penugasan <font color="red">*</font></label>
                  <input class="form-control yearpicker" id="tahun_mulai" placeholder="Tahun Penugasan" type="text" name="tahun_mulai" value="<?php echo @$peserta_tks_detail[0]['tahun_mulai'] ?>" required readonly>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="tahun_berakhir">Tahun Berakhir Penugasan <font color="red">*</font></label>
                  <input class="form-control yearpicker" id="tahun_berakhir" placeholder="Tahun Berakhir Penugasan" type="text" name="tahun_berakhir" value="<?php echo @$peserta_tks_detail[0]['tahun_berakhir'] ?>" required readonly>
                </div>
              </div>  

              <div class="form-group">
                <label for="nama_kelompok_usaha">Nama Kelompok Usaha <font color="red">*</font></label>
                <input class="form-control" id="nama_kelompok_usaha" placeholder="Nama Kelompok Usaha" type="text" name="nama_kelompok_usaha" value="<?php echo @$peserta_tks_detail[0]['nama_kelompok_usaha'] ?>" required>
              </div>            

              <div class="col-md-6">
                <div class="form-group">
                  <label for="province_kelompok_id">Provinsi Kelompok Usaha  <font color="red">*</font></label>
                  <select id="province_kelompok_id" name="province_kelompok_id" class="form-control select2"></select>
                </div>              
              </div>

              <div class="col-md-6">
                <div class="form-group" id="div_id_id_tempat_lahir">
                  <label for="id_vilage_kelompok_usaha">Kota/Kabupaten Kelompok Usaha <font color="red">*</font></label>
                  <select id="id_vilage_kelompok_usaha" name="id_vilage_kelompok_usaha" class="form-control select2"></select>
                </div>
              </div>

              <div class="form-group">
                <label for="alamat_kelompok_usaha">Alamat Kelompok Usaha </label>
                <textarea id="alamat_kelompok_usaha" name="alamat_kelompok_usaha" class="form-control" rows="3" placeholder="Alamat Kelompok Usaha" style="resize: none;" ><?php echo @$peserta_tks_detail[0]['alamat_kelompok_usaha'] ?></textarea>
              </div>

              <div class="form-group">
                <label for="deskripsi_kelompok_usaha">Deskrpsi Kelompok Usaha </label>
                <textarea id="deskripsi_kelompok_usaha" name="deskripsi_kelompok_usaha" class="form-control" rows="3" placeholder="Deskrpsi Kelompok Usaha" style="resize: none;" ><?php echo @$peserta_tks_detail[0]['deskripsi_kelompok_usaha'] ?></textarea>
              </div>


            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box">
            <div class="form-group">
              <span id="text_submit"></span>
              <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success">
              <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a>
            </div>
          </div>
        </div>

      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    // akses();
    // getopendata();
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask();

    var target_prov_lahir = "#province_lahir_id";
    var target_prov_tinggal = "#province_tinggal_id";
    var target_prov_kelompok = "#province_kelompok_id";

    get_provinsi(target_prov_lahir);
    get_provinsi(target_prov_tinggal);
    get_provinsi(target_prov_kelompok);

    $('[name="province_lahir_id"]').change(function(){
      var target_kab_lahir = "#id_tempat_lahir";
      var source_kab_lahir = "#province_lahir_id";

      get_kabupaten(target_kab_lahir,source_kab_lahir);
    });

    $('[name="province_tinggal_id"]').change(function(){
      var target_kab_tinggal = "#id_vilage_alamat";
      var source_kab_tinggal = "#province_tinggal_id";

      get_kabupaten(target_kab_tinggal,source_kab_tinggal);
    });

    $('[name="province_kelompok_id"]').change(function(){
      var target_kab_kelompok = "#id_vilage_kelompok_usaha";
      var source_kab_kelompok = "#province_kelompok_id";

      get_kabupaten(target_kab_kelompok,source_kab_kelompok);
    });

    <?php if (strtolower($proses) == "ubah") { ?>
      // Tempat Lahir
      var target_kab_lahir = "#id_tempat_lahir";
      var source_kab_lahir = "#province_lahir_id";

      $("#province_lahir_id").val("<?php echo @$peserta_tks_detail[0]['prov_lahir_id'] ?>");
      get_kabupaten(target_kab_lahir,source_kab_lahir);
      $("#id_tempat_lahir").val("<?php echo @$peserta_tks_detail[0]['kab_lahir_id'] ?>");

      // Alamat
      var target_kab_tinggal = "#id_vilage_alamat";
      var source_kab_tinggal = "#province_tinggal_id";

      $("#province_tinggal_id").val("<?php echo @$peserta_tks_detail[0]['prov_alamat_id'] ?>");
      get_kabupaten(target_kab_tinggal,source_kab_tinggal);
      $("#id_vilage_alamat").val("<?php echo @$peserta_tks_detail[0]['kab_alamat_id'] ?>");

      // Kelompok
      var target_kab_kelompok = "#id_vilage_kelompok_usaha";
      var source_kab_kelompok = "#province_kelompok_id";

      $("#province_kelompok_id").val("<?php echo @$peserta_tks_detail[0]['prov_kelompok_id'] ?>");
      get_kabupaten(target_kab_kelompok,source_kab_kelompok);
      $("#id_vilage_kelompok_usaha").val("<?php echo @$peserta_tks_detail[0]['kab_kelompok_id'] ?>");
    <?php } ?>
});

function get_provinsi(target) {
  $.ajax({ url : "<?php echo base_url('admin/ajax/ajax_provinsi')?>",
      type: "GET",
      dataType: "JSON",
      async: false,
      success: function(data){
        $(target).empty();
        $(target).append('<option value="">Pilih Provinsi</option>');
        for (var i = 0; i < data.provinsi.length; i++) {
          $(target).append('<option value=' + data.provinsi[i].id + '>' + data.provinsi[i].name + '</option>');
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
          alert('Gagal memperoleh data provinsi');
      }
  });
}

function get_kabupaten(target,source) {
  $.ajax({ url : "<?php echo base_url('admin/ajax/ajax_kabupaten')?>",
    type: "GET",
    data:{ province_id : $(source).val() },
    async: false,
    dataType: "JSON",
    success: function(data){
      $(target).empty();
      $(target).append('<option value="">Pilih Kota/Kabupaten</option>');
      for (var i = 0; i < data.kabupaten.length; i++) {
        $(target).append('<option value=' + data.kabupaten[i].id + '>' + data.kabupaten[i].name + '</option>');
      }
    },
    error: function (jqXHR, textStatus, errorThrown){
        alert('Gagal memperoleh data kabupaten/kota');
    }
  });
}

function function_name(argument) {
  // body...
}
// $('#submit_btn').on('click',function () {
//   $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
//   // $('#submit_btn').attr('disabled',true); //set button disable
//   $('#submit_btn').attr('style','display:none;'); // hide button
//   $('#cancle_btn').attr('style','display:none;'); // hide button
// });

$( "form" ).submit(function(e) {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
  // if ($('#hak_akses').val() == 1) {
  //   return;
  // } else {
  //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
  //     return;
  //   } else {
  //     alert('Hak akses harus dipilih!!!');
  //     e.preventDefault(e);
  //   }
  // }  
});

$('.datepicker').datepicker({
  endDate:'-20y',
  format: "yyyy-mm-dd",
  autoclose: true
});

$('.yearpicker').datepicker({
  format: "yyyy",
  viewMode: "years", 
  minViewMode: "years",
  startDate: '2000',
  endDate: new Date(),
  autoclose: true
});

</script>