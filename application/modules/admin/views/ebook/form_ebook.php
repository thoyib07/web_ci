<?php 
// var_dump($action); 
?>
<script src="<?php echo base_url('assets/plugins/'); ?>pdf/pdf.js"></script>
<div id="area_input_new" style="display:none"></div>
<div id="content">
  <section class="content-header">
    <h1><?php echo $title?> <small> <b>Dashboard</b> Control panel</small> </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('admin/ebook') ?>"> Ebook</a></li>
        <li class="active"><?php echo ucwords($proses); ?> Ebook</li>
      </ol>
  </section>
  <section class='content'>
    <div class='row'>    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
            <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <strong><?= $this->session->flashdata('error') ?></strong>
              </div>
            <?php } ?>

                <input type="hidden" name="id_ebook" id="id_ebook" value="<?php echo @$ebook_detail[0]['id_ebook'] ?>">
                <div class="form-group">
                  <label for="judul_ebook">Judul Ebook <font color="red">*</font></label>
                  <input class="form-control" id="judul_ebook" placeholder="Judul Ebook" type="text" name="judul_ebook" value="<?php echo @$ebook_detail[0]['judul_ebook'] ?>" required>
                </div>

                <div class="form-group">
                  <label for="ebook">Ebook <?php if (strtolower($proses) !== "ubah") { ?><font color="red">*</font><?php } ?></label>
                  <input id="ebook" placeholder="Ebook" type="file" name="ebook" accept="application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/pdf" <?php if (strtolower($proses) !== "ubah") { echo "required"; }?>>
                  <!-- <br><p><b>Preview Gambar</b><br> -->
                  <div class="preview" id="preview"> 
                  <?php if (strrpos(@$ebook_detail[0]['ebook'], ".pdf")) { ?>
                    <iframe id="prev_ebook" class="prev_ebook embed-responsive-item" frameborder="0" scrolling="yes" src="<?php echo base_url(@$ebook_detail[0]['ebook']); ?>" <?php if (strtolower($proses) !== "ubah") { echo 'style="display: none;"'; } else { echo 'style="width: 100%;height: 500px;"'; } ?> ></iframe>
                  <?php } else { ?>
                    <iframe id="prev_ebook" class="prev_ebook embed-responsive-item" frameborder="0" scrolling="yes" src="<?php echo "https://docs.google.com/viewer?url=".base_url(@$ebook_detail[0]['ebook'])."&embedded=true"; ?>" <?php if (strtolower($proses) !== "ubah") { echo 'style="display: none;"'; } else { echo 'style="width: 100%;height: 500px;"'; } ?> ></iframe>
                  <?php } ?> 
                    
                    <input type="hidden" name="old_ebook" id="old_ebook" value="<?php echo @$ebook_detail[0]['ebook']; ?>" >
                  </div>
                </div>

              <div class="clearfix"></div>
              <div class="form-group">
                <span id="text_submit"></span>
                <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success">
                <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    // akses();
    // getopendata();
    // Multiple images preview in browser
    // var imagesPreview = function(input, placeToInsertImagePreview) {
    //     if (input.files) {
    //         var filesAmount = input.files.length;
    //         for (i = 0; i < filesAmount; i++) {
    //             var reader = new FileReader();
    //             reader.onload = function(event) {
    //                 // console.log(event);
    //                 $($.parseHTML('<img height="30%" width="100%"><br><br><br>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
    //             }
    //             reader.readAsDataURL(input.files[i]);
    //         }
    //     }
    // };

    // $('#ebook').on('change', function() {
    //   var myNode = document.getElementById("preview");
    //   // var myNode =  $('#preview')[0];
    //   myNode.innerHTML = '';
    //   imagesPreview(this, 'div.preview');
    // });
});

// function hide_img() {
//   $("#div_gambar_old").hide();
// }

$('#submit_btn').on('click',function () {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
});

$( "form" ).submit(function(e) {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
  // if ($('#hak_akses').val() == 1) {
  //   return;
  // } else {
  //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
  //     return;
  //   } else {
  //     alert('Hak akses harus dipilih!!!');
  //     e.preventDefault(e);
  //   }
  // }  
});

$(function() {
    
});
</script>