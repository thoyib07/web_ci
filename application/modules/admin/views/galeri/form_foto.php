<?php 
// var_dump($action); 
?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>bower_components/ckeditor/dataTables.bootstrap.css"> -->
<script src="<?php echo base_url('assets/admin/'); ?>bower_components/ckeditor/ckeditor.js"></script>
<div id="area_input_new" style="display:none"></div>
<div id="content">
  <section class="content-header">
    <h1><?php echo $title?> <small> <b>Dashboard</b> Control panel</small> </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('admin/galeri') ?>"> Galeri</a></li>
        <li class="active"><?php echo ucwords($proses); ?> Galeri</a></li>
      </ol>
  </section>
  <section class='content'>
    <div class='row'>    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
            <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <strong><?= $this->session->flashdata('error') ?></strong>
              </div>
            <?php } ?>
                <!-- <input type="hidden" name="id_gambar_album" id="id_gambar_album" value="<?php echo @$galeri_detail[0]['id_gambar_album'] ?>"> -->
                <div class="form-group">
                  <label for="judul_album">Judul Album Galeri <font color="red">*</font></label>
                  <select id="judul_album" name="judul_album" class="form-control">
                    <?php foreach ($judul_album as $v_ja) { ?>
                      <option value="<?php echo @$v_ja['id_gambar_album']; ?>" <?php if (@$galeri_detail[0]['id_gambar_album'] == @$v_ja['id_gambar_album'] ) { echo "selected"; } ?> ><?php echo strtoupper(@$v_ja['judul_album']); ?> </option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="gambar">Gambar <?php if (strtolower($proses) !== "ubah") { ?><font color="red">*</font><?php } ?></label>
                  <input id="gambar" placeholder="Gambar" type="file" name="gambar[]" accept="image/x-png,image/jpeg" required multiple >

                  <font color="red">NB. Untuk Memilih Gambar Lebih Dari 1 Tekan 'Ctrl' Saat memilih</font>
                  <br><p><b>Preview Gambar</b><br>
                  <div class="preview col-md-3" id="preview">                  
                  </div>
                </div>
              <div class="clearfix"></div>
              <div class="form-group">
                <span id="text_submit"></span>
                <input type="submit" id="submit_btn" name="submit" value="<?php echo $proses; ?>" class="btn btn-success">
                <a href="<?php echo $back; ?>" id="cancle_btn" class="btn btn-warning">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    // akses();
    // getopendata();
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    // console.log(event);
                    $($.parseHTML('<img height="30%" width="100%"><br><br><br>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('#gambar').on('change', function() {
      var myNode = document.getElementById("preview");
      // var myNode =  $('#preview')[0];
      myNode.innerHTML = '';
      imagesPreview(this, 'div.preview');
    });
});

// function hide_img() {
//   $("#div_gambar_old").hide();
// }

$('#submit_btn').on('click',function () {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button
});

$( "form" ).submit(function(e) {
  $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
  // $('#submit_btn').attr('disabled',true); //set button disable
  $('#submit_btn').attr('style','display:none;'); // hide button
  $('#cancle_btn').attr('style','display:none;'); // hide button  
  // debugger;
  // if ($('#hak_akses').val() == 1) {
  //   return;
  // } else {
  //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
  //     return;
  //   } else {
  //     alert('Hak akses harus dipilih!!!');
  //     e.preventDefault(e);
  //   }
  // }  
});

$(function() {
    
});
</script>