<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Sistem Informasi Statistik Daerah
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> <?php echo ucwords($page['p']); ?></a></li>
        <li class="active"><?php echo ucwords($page['c']); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h1 class="box-title"> Slider </h1>
        </div>
        <div class="box-body">
          <!--
          <div class="col-md-2">
            <a href="<?php echo base_url('admin/slider/add'); ?>"><button class="btn btn-block btn-primary fa fa-plus" type="button" title="Tambah Data"></button></a>
          </div>
          -->
          <div class="clear"></div>
            <table id="example" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <td>No</td>
                  <td>Nama</td>
                  <td>Gambar</td>
                  <td>Status</td>
                  <td>Perintah</td>
                </tr>
              </thead>
              <tbody>
                <?php $no=0; foreach ($slider as $s) { ?>
                  <tr>
                    <td><?php echo $no=$no+1; ?></td>
                    <td><?php echo $s->nama; ?></td>
                    <td>
                      <?php if (isset($s->image)) { ?>
                        <img src="<?php echo base_url('assets/uploads/slider/').''.$s->image; ?>"  width="200">                        
                      <?php } ?>                       
                    </td>
                    <td>
                      <?php if ($s->status == 1) { ?>
                        <font color="Grean"><h5>Aktif</h5></font>
                      <?php } else { ?>
                        <font color="Red"><h5>Tidak Aktif</h5></font>
                      <?php } ?>
                    </td>
                    <td>
                      <a href="<?php echo base_url('admin/slider/edit').'/'.$s->id; ?>"><button class="btn btn-block btn-warning fa fa-pencil" type="button" title="Ubah Data"></button></a>
                      <?php if ($s->status == 1) { ?>
                        <a href="<?php echo base_url('admin/slider/delete').'/'.$s->id; ?>"><button class="btn btn-block btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data"></button></a>
                      <?php } else { ?>
                        <a href="<?php echo base_url('admin/slider/restore').'/'.$s->id; ?>"><button class="btn btn-block btn-success fa fa-power-off" type="button" title="Aktifkan Data"></button></a>
                      <?php } ?>
                      
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->