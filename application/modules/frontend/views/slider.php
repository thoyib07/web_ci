<div class="hero" id="highlighted">
  <div class="inner">
    <!--Slideshow-->
    <div id="highlighted-slider" class="container">
      <div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp"}'>
        <!--Slideshow content-->
        <!--Slide 1-->
        <?php $i = 1; foreach ($slider as $s) { ?>
        <div class="item">
          <div class="row">
            <div class="col-md-6 <?php if($i%2 == 0){ echo "col-md-push-6"; } else { echo "text-right-md"; } ?> item-caption">
              <h2 class="h1 text-weight-light">
                <a href="<?php echo base_url('detail_berita/').$s['id_berita']; ?>"><span class="text-primary"><?php echo ucwords($s['judul']); ?></span></a>
              </h2>
              <h4>
                <?php echo $s['isi']; ?>
              </h4>
            </div>
            <div class="col-md-6 <?php if($i%2 == 0){ echo "col-md-pull-6"; } ?> hidden-xs">
              <a href="<?php echo base_url('detail_berita/').$s['id_berita']; ?>"><img src="<?php echo base_url($s['gambar']); ?>" alt="<?php echo $s['judul']; ?>" class="center-block img-responsive slider-img"></a>
            </div>
          </div>
        </div>
        <?php $i++; } ?>

      </div>
    </div>
  </div>
</div>