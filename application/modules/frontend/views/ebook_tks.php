<style type="text/css">
  .img_loadmore:hover {
    cursor: pointer;
  }
</style>
<div class="container" id="about">
  <div class="row">
    <!--main content-->
    <div class="col-md-12">
      <div class="page-header">
        <h1>
            Ebook TKS
          </h1>
      </div>
      <div class="block block-border-bottom-grey block-pd-sm" id="list_box">
        <?php $i = 1; foreach ($daftar_ebook as $key => $val) { ?>
          <div class="col-lg-4 col-md-4" style="margin-top: 30px;">
            <div class="item">
              <a href="<?php echo base_url($val['ebook']); ?>" class="overlay-wrapper" target="_blank">
                <div style="width: 100%; max-height: 250px; float: left; overflow: hidden;">
                  <img src="<?php echo base_url('assets/img/ebook.png'); ?>" alt="<?php echo $val['judul_ebook']; ?>" class="img-responsive underlay" width="150" >
                </div>
              </a>
              <div class="item-details bg-noise" style="padding: 1em; background: #f3f3f3; border-bottom: 1px solid #bfbfbf;">
                <h4 class="item-title">
                  <a href="<?php echo base_url($val['ebook']); ?>" target="_blank"><?php echo $val['judul_ebook']; ?></a>
                </h4>
                <a href="<?php echo base_url($val['ebook']); ?>" class="btn btn-more" target="_blank"><i class="fa fa-download"></i>Unduh Ebook</a>
              </div>
            </div>
          </div>
          <?php if ($i == 3) { $i=0; ?>
            <div class="clearfix"></div>
          <?php } ?>        
        <?php $i++; } ?>

      </div>
      <div id="loadmore" style="margin-left: 50%;">
        <!-- <button onclick="loadmore(<?php echo $loadmore['start'].','.$loadmore['get']; ?>);"> -->
          <!-- <a href="#"> -->
            <img src="<?php echo base_url('assets/img/loadmore2.png') ?>" width="50" onclick="loadmore(<?php echo $loadmore['start'].','.$loadmore['get']; ?>);" class="img_loadmore">
          <!-- </a> -->
        <!-- </button> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function loadmore(start,get) {
    $.ajax({ 
      url : "<?php echo $loadmore['url']; ?>",
      type : 'GET',
      data : {start : start, get : get},
      dataType : 'JSON',
      success: function(data) {
        // console.log(data.html);
        $('#list_box').append(data.html);
        if (data.html !== "") {
          $('#loadmore').html('');
          $('#loadmore').html('<img src="<?php echo base_url('assets/img/loadmore2.png') ?>"  width="50" onclick="loadmore('+(start+get)+','+get+');"  class="img_loadmore">');
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        swal("Error!", "Aplikasi gagal mendapatkan data!", "error");
      }
    });
  }
</script>