<style type="text/css">
  .img_loadmore:hover {
    cursor: pointer;
  }
</style>

<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>admin/bower_components/datatables/jquery.dataTables.min.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>admin/bower_components/datatables/dataTables.bootstrap.css">
<script type="text/javascript" language="javascript" src="<?php echo base_url('assets/'); ?>admin/bower_components/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url('assets/'); ?>admin/bower_components/datatables/dataTables.bootstrap.min.js"></script>

<script src="<?php echo base_url('assets/plugins/highmaps/'); ?>highmaps.js"></script>
<script src= "<?php echo base_url('assets/plugins/highmaps/'); ?>id-all.js"></script>
<div class="container" id="about">
  <div class="row">
    <!--main content-->
    <div class="col-md-12">
      <div class="page-header">
        <h1>
          <?php echo @$page; ?>
        </h1>
      </div>
      <div class="block block-border-bottom-grey block-pd-sm" id="list_box">
        <div id="maps"></div>
        <div id="data_table">
          <table id="peserta_tks" class="table table-bordered text-center" style="width: 100% !important;">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama</td>
                <td>Status Pendidikan</td>
                <td>Nama Kelompok</td>
                <td>Tahun Mengabdi</td>
              </tr>
            </thead>
          </table>
        </div>
        <div class="clearfix"></div>
      </div>
     <!--  <div id="loadmore" style="margin-left: 50%;">
            <img src="<?php echo base_url('assets/img/loadmore2.png') ?>" width="50" onclick="loadmore(<?php echo $loadmore['start'].','.$loadmore['get'].','.$loadmore['id_jenis_berita']; ?>);" class="img_loadmore">
      </div> -->
    </div>
  </div>
</div>
<script type="text/javascript">
var table;

$(document).ready(function() {
    
    //datatables
    //function kategori() {
      table = $('#peserta_tks').DataTable({ 
 
        "processing"  : true, //Feature control the processing indicator.
        "serverSide"  : true, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 0.5 * 1000,
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/admin/ajax_list?type=peserta_tks')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [  ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      });
    //}
    
    
    // Setiap 1 menit perbarui data
    setInterval( function () {
      table.ajax.reload(null,false);
    }, 60 * 1000 );
 
});
</script>

<script>
  $(function() {    
    var data = <?php echo $daftar_provinsi; ?>;

    // Initiate the chart
    $('#maps').highcharts('Map', {
      legend: {
            enabled: false
          },


        title : {
            text : 'Jumlah Peserta TKS Indonesia Tahun '+<?php echo date("Y"); ?>
        },

        subtitle : {
            text : '(Sumber : Database Peserta TKS)'
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'top'
            }
        },
        series : [{
            data : data,
            mapData: Highcharts.maps['countries/id/id-all'],
            joinBy: 'hc-key',
            name: 'Jumlah Peserta TKS',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            },
      tooltip: {
                    valueSuffix: ' Orang'
                }
        }]
    });
  });
  </script>