<!DOCTYPE html>
<html lang="en">
<?php include 'parts/head.php'; ?>
<body class="page-index has-hero">
  <!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->
  <div id="background-wrapper" class="bromo" data-stellar-background-ratio="0.1">
    <?php include 'parts/menu.php'; ?>
    <?php  if (isset($slider)) { include 'slider.php'; } ?>
  </div>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <?php  if (isset($content)) { ?>
    <?php  echo @$content ?>      
    <?php  } else { ?>
      <!-- Mission Statement -->
      <div class="mission text-center block block-pd-sm block-bg-noise">
        <div class="container">
          <h2 class="text-shadow-white">
            <b>Program Pendayagunaan Tenaga Kerja Sukarela (TKS)</b> adalah salah satu program prioritas Kementerian Ketenagakerjaan yang bertujuan untuk mendayagunakan para sarjana dalam kegiatan pemberdayaan masyarakat, khususnya pendampingan kepada kelompok-kelompok masyarakat di perdesaan.<br>
            <a href="<?php echo base_url('profil_tks/'); ?>" class="btn btn-more"><i class="fa fa-plus"></i>Baca Selengkapnya</a>
          </h2>
        </div>
      </div>
    <?php  } ?>
    <?php  if (isset($list_berita)) { include 'list_berita.php'; } ?>
    <?php  if (isset($list_galeri)) { include 'list_galeri.php'; } ?>
    <?php  if (isset($list_ebook)) { include 'list_ebook.php'; } ?>
  </div>

<?php include 'parts/foot.php'; ?>
<?php include 'parts/js.php'; ?>

</body>

</html>
