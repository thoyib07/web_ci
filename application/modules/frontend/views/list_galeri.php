<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
<style type="text/css">
  /* Style the Image Used to Trigger the Modal */
  #myImg {
      border-radius: 5px;
      cursor: pointer;
      transition: 0.3s;
  }

  #myImg:hover {opacity: 0.7;}

  /* The Modal (background) */
  .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }

  /* Modal Content (Image) */
  .modal-content {
      margin: auto;
      display: block;
      width: 80%;
      max-width: 700px;
  }

  /* Caption of Modal Image (Image Text) - Same Width as the Image */
  #caption {
      margin: auto;
      display: block;
      width: 80%;
      max-width: 700px;
      text-align: center;
      color: #ccc;
      padding: 10px 0;
      height: 150px;
  }

  /* Add Animation - Zoom in the Modal */
  .modal-content, #caption {
      animation-name: zoom;
      animation-duration: 0.6s;
  }

  @keyframes zoom {
      from {transform:scale(0)}
      to {transform:scale(1)}
  }

  /* The Close Button */
  .close {
      position: absolute;
      top: 15px;
      right: 35px;
      color: #f1f1f1;
      font-size: 40px;
      font-weight: bold;
      transition: 0.3s;
  }

  .close:hover,
  .close:focus {
      color: #bbb;
      text-decoration: none;
      cursor: pointer;
  }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
      .modal-content {
          width: 100%;
      }
  } 
</style>
<script type="text/javascript">
  // Get the modal
  var modal = document.getElementById('myModal');

  // Get the image and insert it inside the modal - use its "alt" text as a caption
  // var img = document.getElementsByClassName('myImg');
  var modalImg = document.getElementById("img01");
  // var modalImg = document.getElementsByClassName("imgPop");
  var captionText = document.getElementById("caption");
  // img.onclick = function(){
  //     modal.style.display = "block";
  //     modalImg.src = this.src;
  //     captionText.innerHTML = this.alt;
  // }

  function imgPop_up(src,caption) {
    modal.style.display = "block";
    modalImg.src = src;
    captionText.innerHTML = caption;
  }

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  } 
</script>
<!--Showcase-->
<div class="showcase block block-border-bottom-grey">
  <div class="container">
    <h2 class="block-title">
        Galeri TKS
      </h2>
    <p>Galeri TKS terbaru.</p>
    <div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "itemsScaleUp":true}'>
      
      <?php foreach ($list_galeri as $key => $val) { ?>
        <div class="item">
          <!-- <a href="#" class="overlay-wrapper"> -->
              <img id="myImg" src="<?php echo base_url($val['gambar']); ?>" alt="<?php echo $val['judul_album']; ?>" class="img-responsive underlay myImg" onclick="imgPop_up(<?php echo "'".base_url($val['gambar'])."','".$val['judul_album']."'"; ?>);" width="350" >
              <!-- <span class="overlay">
                <span class="overlay-content"> <span class="h4"><?php echo $val['judul_album']; ?></span> </span>
              </span> -->
            <!-- </a> -->
          <div class="item-details bg-noise">
            <h4 class="item-title">
                <a href="#"><?php echo $val['judul_album']; ?></a>
              </h4>
          </div>
        </div>
      <?php } ?>

    </div>
  </div>
</div>