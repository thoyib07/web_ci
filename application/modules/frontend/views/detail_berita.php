<?php $publish_date = strtotime(@$detail[0]['cdd']); ?>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58621f526e03a227"></script>
<div class="container" id="about">
  <div class="row">
    <!--main content-->
    <!-- <div class="col-md-12"> -->
    <div class="col-md-9 col-md-push-3">
      <div class="page-header">
        <h1>
          <?php echo @$detail[0]['judul']; ?>
        </h1>
        <small><?php echo date("d M Y H:i:s", $publish_date); ?></small>
      </div>
      <div class="block block-border-bottom-grey block-pd-sm">
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_inline_share_toolbox"></div>
        <img width="300px" height="300px" src="<?php echo base_url(@$detail[0]['gambar']);?>" class="img-responsive img-thumbnail pull-right m-l m-b">
        <?php echo @$detail[0]['isi']; ?>
      </div>
    </div>
    <div class="col-md-3 col-md-pull-9 sidebar visible-md-block visible-lg-block"><ul class="nav nav-pills nav-stacked"><li class="active"><a href="#" class="first"> Detail Berita </a></li></ul></div>
  </div>
  <!--Showcase-->
  <div class="showcase block block-border-bottom-grey">
    <div class="container">
      <h2 class="block-title">
        Berita TKS Lainnya
      </h2>
      <div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "itemsScaleUp":true}'>
        <?php foreach ($berita_lainnya as $bl) { ?>
        <div class="item">
          <a href="<?php echo base_url('detail_berita/').$bl['id_berita']; ?>" class="overlay-wrapper">
              <img src="<?php echo base_url($bl['gambar']); ?>" alt="<?php echo $bl['judul']; ?>" style="max-width: 275px;" class="<?php echo base_url($bl['gambar']); ?>-responsive underlay">
              <span class="overlay">
                <span class="overlay-content"> <span class="h4"><?php echo $bl['judul']; ?></span> </span>
              </span>
            </a>
          <div class="item-details bg-noise">
            <h4 class="item-title">
                <a href="<?php echo base_url('detail_berita/').$bl['id_berita']; ?>"><?php echo $bl['judul']; ?></a>
              </h4>
            <a href="<?php echo base_url('detail_berita/').$bl['id_berita']; ?>" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>