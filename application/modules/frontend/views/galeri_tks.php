<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close" id="span_close">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
<style type="text/css">
  .img_loadmore:hover {
    cursor: pointer;
  }
  /* Style the Image Used to Trigger the Modal */
  #myImg {
      border-radius: 5px;
      cursor: pointer;
      transition: 0.3s;
  }

  #myImg:hover {opacity: 0.7;}

  /* The Modal (background) */
  .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }

  /* Modal Content (Image) */
  .modal-content {
      margin: auto;
      display: block;
      width: 80%;
      max-width: 700px;
  }

  /* Caption of Modal Image (Image Text) - Same Width as the Image */
  #caption {
      margin: auto;
      display: block;
      width: 80%;
      max-width: 700px;
      text-align: center;
      color: #ccc;
      padding: 10px 0;
      height: 150px;
  }

  /* Add Animation - Zoom in the Modal */
  .modal-content, #caption {
      animation-name: zoom;
      animation-duration: 0.6s;
  }

  @keyframes zoom {
      from {transform:scale(0)}
      to {transform:scale(1)}
  }

  /* The Close Button */
  .close {
      position: absolute;
      top: 15px;
      right: 35px;
      color: #f1f1f1;
      font-size: 40px;
      font-weight: bold;
      transition: 0.3s;
  }

  .close:hover,
  .close:focus {
      color: #bbb;
      text-decoration: none;
      cursor: pointer;
  }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
      .modal-content {
          width: 100%;
      }
  } 
</style>
<script type="text/javascript">
  // Get the modal
  var modal = document.getElementById('myModal');

  // Get the image and insert it inside the modal - use its "alt" text as a caption
  // var img = document.getElementsByClassName('myImg');
  var modalImg = document.getElementById("img01");
  // var modalImg = document.getElementsByClassName("imgPop");
  var captionText = document.getElementById("caption");
  // Get the <span> element that closes the modal
  var span = document.getElementById("span_close");
  // img.onclick = function(){
  //     modal.style.display = "block";
  //     modalImg.src = this.src;
  //     captionText.innerHTML = this.alt;
  // }

  function imgPop_up(src,caption) {
    modal.style.display = "block";
    modalImg.src = src;
    captionText.innerHTML = caption;
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  } 
</script>
<div class="container" id="about">
  <div class="row">
    <!--main content-->
    <div class="col-md-12">
      <div class="page-header">
        <h1>
            Galeri TKS
          </h1>
      </div>
      <div class="block block-border-bottom-grey block-pd-sm" id="list_box">
        <?php $i = 1; foreach ($daftar_galeri as $key => $val) { ?>
          <div class="col-lg-4 col-md-4" style="margin-top: 30px;">
            <div style="width: 100%; max-height: 250px; float: left; overflow: hidden;">
              <img src="<?php echo base_url($val['gambar']); ?>" alt="<?php echo $val['judul_album']; ?>" class="img-responsive underlay" onclick="imgPop_up(<?php echo "'".base_url($val['gambar'])."','".$val['judul_album']."'"; ?>);">
            </div>
            <div class="item-details bg-noise" style="padding: 1em; background: #f3f3f3; border-bottom: 1px solid #bfbfbf;">
              <h4 class="item-title">
                <p style="margin-left: 10px;10px;color: #343331;"><b><?php echo $val['judul_album']; ?></b></p>
              </h4>
            </div>
          </div>
          <?php if ($i == 3) { $i=0; ?>
            <div class="clearfix"></div>
          <?php } ?>        
        <?php $i++; } ?>

      </div>
      <div id="loadmore" style="margin-left: 50%;">
        <!-- <button onclick="loadmore(<?php echo $loadmore['start'].','.$loadmore['get']; ?>);"> -->
          <!-- <a href="#"> -->
            <img src="<?php echo base_url('assets/img/loadmore2.png') ?>" width="50" onclick="loadmore(<?php echo $loadmore['start'].','.$loadmore['get']; ?>);" class="img_loadmore">
          <!-- </a> -->
        <!-- </button> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function loadmore(start,get) {
    $.ajax({ 
      url : "<?php echo $loadmore['url']; ?>",
      type : 'GET',
      data : {start : start, get : get},
      dataType : 'JSON',
      success: function(data) {
        // console.log(data.html);
        $('#list_box').append(data.html);
        if (data.html !== "") {
          $('#loadmore').html('');
          $('#loadmore').html('<img src="<?php echo base_url('assets/img/loadmore2.png') ?>"  width="50" onclick="loadmore('+(start+get)+','+get+');"  class="img_loadmore">');
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        swal("Error!", "Aplikasi gagal mendapatkan data!", "error");
      }
    });
  }
</script>