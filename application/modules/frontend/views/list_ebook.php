<!--Showcase-->
<div class="showcase block block-border-bottom-grey">
  <div class="container">
    <h2 class="block-title">
        Ebook TKS
      </h2>
    <p>Ebook TKS terbaru.</p>
    <div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "itemsScaleUp":true}'>

      <?php foreach ($list_ebook as $key => $val) { ?>
        <div class="item">
          <a href="#" class="overlay-wrapper">
              <!-- <img src="<?php echo base_url('assets/img/ebook.png'); ?>" alt="<?php echo $val['judul_ebook']; ?>" class="img-responsive underlay" width="150" > -->
              <span class="fa-stack fa-3x" style="margin-left: 20%;">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-book fa-stack-1x fa-inverse"></i>
              </span>
              <span class="overlay">
                <span class="overlay-content"> <span class="h4"><?php echo $val['judul_ebook']; ?></span> </span>
              </span>
            </a>
          <div class="item-details bg-noise">
            <h4 class="item-title">
                <a href="#"><?php echo $val['judul_ebook']; ?></a>
              </h4>
            <a href="<?php echo base_url($val['ebook']); ?>" class="btn btn-more" target="_blank"><i class="fa fa-download"></i>Unduh Ebook</a>
          </div>
        </div>
      <?php } ?>

    </div>
  </div>
</div>