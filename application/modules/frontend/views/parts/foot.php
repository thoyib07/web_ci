<!-- ======== @Region: #footer ======== -->
<footer id="footer" class="block block-bg-grey-dark" data-block-bg-img="<?php echo base_url('assets/img/'); ?>bg_footer-map.png" data-stellar-background-ratio="0.4">
  <div class="container">

    <div class="row" id="contact">

      <div class="col-md-9">
        <address>
            <strong>DIREKTORAT PENGEMBANGAN DAN PERLUASAN KESEMPATAN KERJA DITJEN BINAPENTA DAN PKK<br>Kementerian Ketenagakerjaan Republik Indonesia</strong>
            <br>
            <i class="fa fa-map-pin fa-fw text-primary"></i> Jl. Jend. Gatot Subroto Kav. 51 Lt. IV A Jakarta Pusat, DKI Jakarta Indonesia
            <br>
            <i class="fa fa-phone fa-fw text-primary"></i> 021-5274930 ext. 541
            <br>
            <i class="fa fa-envelope-o fa-fw text-primary"></i> tks.nakertrans@gmail.com
            <br>
          </address>
      </div>
      <div class="col-md-3">
        <h4 class="text-uppercase">
            Follow Us On:
          </h4>
        <!--social media icons-->
        <div class="social-media social-media-stacked">
          <!--@todo: replace with company social media details-->
          <a href="https://www.facebook.com/tksnakertrans" target="_blank"><i class="fa fa-twitter fa-fw"></i> Twitter</a>
          <a href="http://twitter.com/TKSNakertrans" target="_blank"><i class="fa fa-facebook fa-fw"></i> Facebook</a>
        </div>
      </div>

    </div>

    <div class="row subfooter">
      <!--@todo: replace with company copyright details-->
      <div class="col-md-7">
        <p>Copyright © Flexor Theme Used By Thoyib Hidayat</p>
        <div class="credits">
        </div>
      </div>
      <div class="col-md-5">
        <ul class="list-inline pull-right">
          <li><a href="#">Terms</a></li>
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul>
      </div>
    </div>

    <a href="#top" class="scrolltop">Top</a>

  </div>
</footer>