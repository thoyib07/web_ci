<style type="text/css">
  .img_loadmore:hover {
    cursor: pointer;
  }
</style>
<div class="container" id="about">
  <div class="row">
    <!--main content-->
    <div class="col-md-12">
      <div class="page-header">
        <h1>
          <?php echo @$page; ?>
        </h1>
      </div>
      <div class="block block-border-bottom-grey block-pd-sm" id="list_box">
        <?php $i = 1; foreach ($daftar_berita as $key => $val) { ?>
          <div class="col-lg-4 col-md-4" style="margin-top: 30px;">
            <div class="item">
              <a href="<?php echo base_url('detail_berita/').$val['id_berita']; ?>" class="overlay-wrapper">
                <div style="width: 100%; max-height: 250px; float: left; overflow: hidden;">
                  <img src="<?php echo base_url($val['gambar']); ?>" alt="<?php echo $val['judul']; ?>" class="img-responsive underlay" >
                </div>
                <div class="item-details bg-noise" style="padding: 1em; background: #f3f3f3; border-bottom: 1px solid #bfbfbf;">
                  <h4 class="item-title">
                      <a href="<?php echo base_url('detail_berita/').$val['id_berita']; ?>"><p style="margin-left: 10px;10px;color: #343331;"><b><?php echo $val['judul']; ?></b></p></a>
                  </h4>

                  <p style="margin-left: 10px;10px;color: #343331;">
                    <?php $leng = 50; if (strlen($val['isi']) < $leng) {
                      echo $val['isi'];
                    } else {
                      echo substr($val['isi'], 0, ($leng-3)).'...</p>';
                    }  ?>
                  </p>
                  <a href="<?php echo base_url('detail_berita/').$val['id_berita']; ?>" class="btn btn-more" style="margin-left:  10px;margin-bottom: 20px;"><i class="fa fa-plus"></i>Baca selengkapnya</a>
                </div>
              </a>
            </div>
          </div>
          <?php if ($i == 3) { $i=0; ?>
            <div class="clearfix"></div>
          <?php } ?>        
        <?php $i++; } ?>

      </div>
      <div id="loadmore" style="margin-left: 50%;">
        <!-- <button onclick="loadmore(<?php echo $loadmore['start'].','.$loadmore['get']; ?>);"> -->
          <!-- <a href="#"> -->
            <img src="<?php echo base_url('assets/img/loadmore2.png') ?>" width="50" onclick="loadmore(<?php echo $loadmore['start'].','.$loadmore['get'].','.$loadmore['id_jenis_berita']; ?>);" class="img_loadmore">
          <!-- </a> -->
        <!-- </button> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function loadmore(start,get,id_jenis_berita) {
    $.ajax({ 
      url : "<?php echo $loadmore['url']; ?>",
      type : 'GET',
      data : {start : start, get : get, id_jenis_berita : id_jenis_berita},
      dataType : 'JSON',
      success: function(data) {
        // console.log(data.html);
        $('#list_box').append(data.html);
        if (data.html !== "") {
          $('#loadmore').html('');
          $('#loadmore').html('<img src="<?php echo base_url('assets/img/loadmore2.png') ?>"  width="50" onclick="loadmore('+(start+get)+','+get+','+id_jenis_berita+');"  class="img_loadmore">');
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        swal("Error!", "Aplikasi gagal mendapatkan data!", "error");
      }
    });
  }
</script>