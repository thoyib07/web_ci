<!--Showcase-->
<div class="showcase block block-border-bottom-grey">
  <div class="container">
    <h2 class="block-title">
        Berita TKS
      </h2>
    <p>Berita TKS terbaru.</p>
    <div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "itemsScaleUp":true}'>
      
      <?php foreach ($list_berita as $key => $val) { ?>
        <div class="item">
          <a href="<?php echo base_url('detail_berita/').$val['id_berita']; ?>" class="overlay-wrapper">
              <img src="<?php echo base_url($val['gambar']); ?>" alt="<?php echo $val['judul']; ?>" class="img-responsive underlay" width="350" >
              <span class="overlay">
                <span class="overlay-content"> <span class="h4"><?php echo $val['judul']; ?></span> </span>
              </span>
            </a>
          <div class="item-details bg-noise">
            <h4 class="item-title">
                <a href="<?php echo base_url('detail_berita/').$val['id_berita']; ?>"><?php echo $val['judul']; ?></a>
              </h4>
            <a href="<?php echo base_url('detail_berita/').$val['id_berita']; ?>" class="btn btn-more"><i class="fa fa-plus"></i>Baca selengkapnya</a>
          </div>
        </div>
      <?php } ?>

    </div>
  </div>
</div>