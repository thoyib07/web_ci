<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
    parent::__construct();
  	$this->load->library(array('session','pagination'));
 		$this->load->model(array('M_admin'));
  	$this->load->helper(array('url','array','download'));
  }

	public function date()
	{
		$date = date('Y-m-d H:i:s');
		return $date;
	}

	public function active_time()
	{
	 $timer = 60 * 60; //60 menit
	 if ($this->session->userdata('login') != 1) {
		 $this->load->view('admin/login');
	 } elseif (time() - $this->session->userdata('time') > $timer) {
		 $session = array('login' => FALSE, 'time' => '');
		 $this->session->set_userdata($session);
		 redirect('admin/index');
	 }
 	}

	public function login()
	{
		$this->load->view('admin/login');
	}

	public function logout()
	{
		$this->session->unset_userdata('time');
 		$this->session->unset_userdata('login');
 		$this->session->sess_destroy();
		redirect(base_url().'admin/');
	}

	public function check()
	{
		$user = $_POST['user'];
		$pass = $_POST['pass'];

		if (($user == 'admin')&&($pass == 'passadmin')) {
			$session = array('login' => true, 'time' => time());
			$this->session->set_userdata($session);
			redirect(base_url().'admin/index');
		} else {
			redirect(base_url().'admin/login');
		}
	}

	public function index()
	{
		session_start();
		$this->active_time();
		if ($this->session->userdata('login') == 1) {
			redirect(base_url().'admin/home');
		} else {
			redirect(base_url().'admin/login');
		}
	}

	public function home()
	{
		$this->active_time();
		$data['page'] 		= 'home';
		$data['title'] 		= 'Admin Microsite NOVAVERSARY29';
		$data['content'] 	= $this->load->view('admin/home',$data,TRUE);
		$this->load->view('admin/layout',$data);
	}

	public function peserta($id=NULL)
	{
		$this->active_time();
		$data['page'] 							= 'peserta';
		$data['title'] 							= 'Admin Microsite NOVAVERSARY29';
		$data['jumlah'] 						= $this->M_admin->c_getUser();

		$config['per_page'] 				= 10;
		$config['base_url'] 				= base_url().'admin/peserta/';
		$config['total_rows'] 			= $data['jumlah'];
		$config['full_tag_open'] 		= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['prev_link'] 				= '&laquo';
		$config['prev_tag_open'] 		= '<li class="prev">';
		$config['prev_tag_close'] 	= '</li>';
		$config['next_link'] 				= '&raquo';
		$config['next_tag_open'] 		= '<li>';
		$config['next_tag_close'] 	= '</li>';
		$config['last_tag_open'] 		= '<li>';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 		= '<li class="active"><a href="#">';
		$config['cur_tag_close'] 		= '</a></li>';
		$config['num_tag_open'] 		= '<li>';
		$config['num_tag_close'] 		= '</li>';

		$data['result'] = $this->M_admin->getUser($config['per_page'],$id);
		if ($id == NULL) {$no = 0;}else {$no = $id;};
		$data['id'] = $no;
		$this->pagination->initialize($config);
		//var_dump($data['result']);die();
		$data['content'] 	= $this->load->view('admin/peserta',$data,TRUE);
		$this->load->view('admin/layout',$data);
	}

	public function kode($id=NULL)
	{
		$this->active_time();
		$data['page'] 							= 'kode';
		$data['title'] 							= 'Admin Microsite NOVAVERSARY29';
		$data['jumlah'] 						= $this->M_admin->c_getKode();

		$config['per_page'] 				= 10;
		$config['base_url'] 				= base_url().'admin/kode/';
		$config['total_rows'] 			= $data['jumlah'];
		$config['full_tag_open'] 		= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['prev_link'] 				= '&laquo';
		$config['prev_tag_open'] 		= '<li class="prev">';
		$config['prev_tag_close'] 	= '</li>';
		$config['next_link'] 				= '&raquo';
		$config['next_tag_open'] 		= '<li>';
		$config['next_tag_close'] 	= '</li>';
		$config['last_tag_open'] 		= '<li>';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 		= '<li class="active"><a href="#">';
		$config['cur_tag_close'] 		= '</a></li>';
		$config['num_tag_open'] 		= '<li>';
		$config['num_tag_close'] 		= '</li>';

		$data['result'] = $this->M_admin->getKode($config['per_page'],$id);
		if ($id == NULL) {$no = 0;}else {$no = $id;};
		$data['id'] = $no;
		$this->pagination->initialize($config);
		//var_dump($data['result']);die();
		$data['content'] 	= $this->load->view('admin/kode',$data,TRUE);
		$this->load->view('admin/layout',$data);
	}

	public function add($table)
	{
		switch ($table) {
			case 'kode':
				if (isset($_POST['submit'])) {
					$data['kode'] 						= $_POST['kode'];
					$data['created_date'] 		= date('Y-m-d h:i:s', strtotime("now"));
					$this->M_admin->addKode($data);
					redirect(base_url().'admin/kode');
				} else {
					$data['page'] = 'kode';
					$data['title'] = 'Admin Microsite NOVAVERSARY29';
					$data['content'] = $this->load->view('admin/add_kode',$data,TRUE);
					$this->load->view('admin/layout',$data);
				}
				break;

			default:
				redirect(base_url().'admin/index');
				break;
		}
	}

	public function edit($table, $id)
	{
		switch ($table) {
			case 'kode':
				if (isset($_POST['submit'])) {
					$data['id'] 		= $id;
					$data['kode'] 	= $_POST['kode'];
					$this->M_admin->editKode($data);
					redirect(base_url().'admin/kode');
				} else {
					$data['page'] = 'kode';
					$data['title'] = 'Admin Microsite NOVAVERSARY29';
					$data['result'] = $this->M_admin->getKodeById($id);
					$data['content'] = $this->load->view('admin/edit_kode',$data,TRUE);
					$this->load->view('admin/layout',$data);
				}
				break;

			default:
				redirect(base_url().'admin/index');
				break;
		}
	}

	public function delete($table,$id)
	{
		switch ($table) {
			case 'peserta':
				$this->M_admin->delUser($id);
				redirect(base_url().'admin/peserta');
				break;
			case 'kode':
				$this->M_admin->delKode($id);
				redirect(base_url().'admin/kode');
				break;

			default:
				redirect(base_url().'admin/index');
				break;
		}

	}

	public function download($value)
	{
		switch ($value) {
			case 'peserta':
				$data['result']	= $this->M_admin->getUser(NULL,NULL);
				//var_dump($data['result']);//die();
				$this->load->view('admin/report_peserta',$data);
				break;
			case 'who':
				$data['result']	= $this->M_admin->getUserWho(NULL,NULL);
				//var_dump($data['result']);//die();
				$this->load->view('admin/report_who',$data);
				break;

			default:
				# code...
				break;
		}
	}

	public function hello()
	{
		$this->load->view('hello');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
