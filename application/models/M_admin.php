<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */ 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($type=null,$id=null) { 
        switch ($type) {
            case 'user':
            case 'del_user':
                $column_order = array('id_user', 'nama'); //set column field database for datatable orderable
                $column_search = array('nama'); //set column field database for datatable searchable 
                $order = array('id_user' => 'desc'); // default order 

                $this->db->select("u.id_user as id, u.nama as nama, l.role as jabatan");
                $this->db->from("m_user as u");
                $this->db->join("m_user_lvl as l","l.id_user_lvl = u.id_user_lvl");
                break;

            case 'berita':
            case 'del_berita':
                $column_order = array('b.id_berita', 'b.gambar', 'jb.jenis_berita', 'b.judul', 'b.cdd',  'b.publish' ); //set column field database for datatable orderable
                $column_search = array('b.judul','b.isi','b.cdd'); //set column field database for datatable searchable 
                $order = array('b.id_berita' => 'desc'); // default order 

                $this->db->select("b.id_berita as id, b.judul, b.gambar, b.isi, b.publish, b.cdd, jb.jenis_berita");
                $this->db->from("t_berita as b");
                $this->db->join("m_jenis_berita as jb","jb.id_jenis_berita = b.id_jenis_berita");
                break;

            case 'galeri':
            case 'del_galeri':
                $column_order = array('gd.id_gambar_detail', 'gd.gambar', 'ga.judul_album', 'gd.cdd'); //set column field database for datatable orderable
                $column_search = array('ga.judul_album','gd.gambar','gd.cdd'); //set column field database for datatable searchable 
                $order = array('gd.id_gambar_detail' => 'desc'); // default order 

                $this->db->select("gd.id_gambar_detail, ga.id_gambar_album, gd.gambar, ga.judul_album, gd.cdd");
                $this->db->from("t_gambar_detail as gd");
                $this->db->join("t_gambar_album as ga","gd.id_gambar_album = ga.id_gambar_album");
                break;

            case 'ebook':
            case 'del_ebook':
                $column_order = array('id_ebook', 'judul_ebook', 'cdd' ); //set column field database for datatable orderable
                $column_search = array('judul_ebook','cdd'); //set column field database for datatable searchable 
                $order = array('id_ebook' => 'desc'); // default order 

                $this->db->select("e.*");
                $this->db->from("t_ebook as e");
                break;

            case 'peserta_tks':
            case 'del_peserta_tks':
                $column_order = array('p.id_peserta_tks', 'p.nama', 'p.status_pendidikan', 'p.nama_kelompok_usaha', 'r_k.name', 'pr_k.name', 'p.tahun_mulai', 'p.cdd' ); //set column field database for datatable orderable
                $column_search = array('p.nama', 'p.status_pendidikan', 'p.nama_kelompok_usaha', 'r_k.name', 'pr_k.name', 'p.tahun_mulai', 'p.cdd'); //set column field database for datatable searchable 
                $order = array('p.id_peserta_tks' => 'desc'); // default order 

                $this->db->select("p.*, r_k.name as kabupaten_kelompok, pr_k.name as provinsi_kelompok");
                $this->db->from("m_peserta_tks as p");
                $this->db->join("regencies as r_k","r_k.id = p.id_vilage_kelompok_usaha");
                $this->db->join("provinces as pr_k","pr_k.id = r_k.province_id");
                $this->db->join("regencies as r_a","r_a.id = p.id_vilage_alamat");
                $this->db->join("provinces as pr_a","pr_a.id = r_a.province_id");
                $this->db->join("regencies as r_l","r_l.id = p.id_tempat_lahir");
                $this->db->join("provinces as pr_l","pr_l.id = r_l.province_id");
                break;
            
            default:
                # code...
                break;
        }
 
        $i = 0;
     
        foreach ($column_search as $item) // loop column 
        {
            if (isset($_POST['search'])) {
                if($_POST['search']['value']) // if datatable send POST for search
                {                     
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
     
                    if(count($column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
        }
        
        if (!isset($order_req)) {
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($order))
            {           
                foreach ($order as $key => $value) {
                    $this->db->order_by($key, $value);
                } 
            }
        }  
    }
 
    public function get_datatables($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where("u.status = 1");
                break;
            case 'del_user':
                $this->db->where("u.status = 1");
                break;            
            case 'berita':
                $this->db->where("b.status = 1");
                break;
            case 'del_berita':
                $this->db->where("b.status = 0");
                break;            
            case 'galeri':
                $this->db->where("gd.status = 1");
                break;
            case 'del_galeri':
                $this->db->where("gd.status = 0");
                break;            
            case 'ebook':
                $this->db->where("e.status = 1");
                break;
            case 'del_ebook':
                $this->db->where("e.status = 0");
                break;            
            case 'peserta_tks':
                $this->db->where("p.status = 1");
                break;
            case 'del_peserta_tks':
                $this->db->where("p.status = 0");
                break;
            
            default:
                # code...
                break;
        }

        if($_POST['length'] != -1){
        	$this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
 
    public function count_filtered($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where("u.status = 1");
                break;
            case 'del_user':
                $this->db->where("u.status = 1");
                break;            
            case 'berita':
                $this->db->where("b.status = 1");
                break;
            case 'del_berita':
                $this->db->where("b.status = 0");
                break;            
            case 'galeri':
                $this->db->where("gd.status = 1");
                break;
            case 'del_galeri':
                $this->db->where("gd.status = 0");
                break;           
            case 'ebook':
                $this->db->where("e.status = 1");
                break;
            case 'del_ebook':
                $this->db->where("e.status = 0");
                break;            
            case 'peserta_tks':
                $this->db->where("p.status = 1");
                break;
            case 'del_peserta_tks':
                $this->db->where("p.status = 0");
                break;
            
            default:
                # code...
                break;
        }

        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where("u.status = 1");
                break;
            case 'del_user':
                $this->db->where("u.status = 1");
                break;            
            case 'berita':
                $this->db->where("b.status = 1");
                break;
            case 'del_berita':
                $this->db->where("b.status = 0");
                break;            
            case 'galeri':
                $this->db->where("gd.status = 1");
                break;
            case 'del_galeri':
                $this->db->where("gd.status = 0");
                break;           
            case 'ebook':
                $this->db->where("e.status = 1");
                break;
            case 'del_ebook':
                $this->db->where("e.status = 0");
                break;            
            case 'peserta_tks':
                $this->db->where("p.status = 1");
                break;
            case 'del_peserta_tks':
                $this->db->where("p.status = 0");
                break;
            
            default:
                # code...
                break;
        }

        return $this->db->count_all_results();
    }

    public function getData($value='') {
        $this->db->select($value['select']);

        if (isset($value['table'])) {
            $this->db->from($value['table']);
        } elseif (isset($value['from'])) {
            $this->db->from($value['from']);
        }

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }
            
        if (isset($value['limit'])) {
                $this->db->limit($value['limit']['0'],$value['limit']['1']);
        }

        if (isset($value['order'])) {
            $this->db->order_by($value['order']['0'],$value['order']['1']);
        }

        if (isset($value['group_by'])) {
            $this->db->group_by($value['group_by']); 
        }
        
        $result = $this->db->get()->result_array();
        return $result;
    }

  	public function addData($value=null) {
		$this->db->insert($value['table'],$value['data']);
 		$id = $this->db->insert_id();
 		return $id;
  	}
    
    public function updateData($value) {
        if (isset($value['where'])) {
            foreach ($value['where'] as $where) {
                $this->db->where($where['0'],$where['1']);
            }
        }
        
        $this->db->update($value['table'], $value['data']);
    }

	public function delById($value) {
   		$data = array(
             'status' => '0',
          );

	    $this->db->where($value['search'], $value['id']);
	    $this->db->update($value['table'], $data);
  	}

    public function restorById($value) {
        $data = array('status' => '1',);

        $this->db->where($value['search'], $value['id']);
        $this->db->update($value['table'], $data);
    }
}
