<div class="container">

  <!-- Main component for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="page-header">
        <a href="<?php echo base_url().'admin/download/peserta'; ?>"><button type="button" name="button" class="btn btn-primary">Download Daftar Peserta</button></a>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Username</th>
          <th>Alamat</th>
          <th>Email</th>
          <th>Telpon</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $key): ?>
          <tr>
            <td><?php echo $id = $id+1; ?></td>
            <td><?php echo strtoupper($key->nama); ?></td>
            <td><?php echo $key->username; ?></td>
            <td><?php echo $key->alamat; ?></td>
            <td><?php echo $key->email; ?></td>
            <td><?php echo $key->telp; ?></td>
            <td><a href="<?php echo base_url().'admin/delete/peserta/'.$key->id; ?>"><button type="button" name="button" class="btn btn-danger">Delete</button></a></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php echo $this->pagination->create_links();
      echo "<br>Total Data : ".$jumlah; ?>
  </div>

</div> <!-- /container -->
