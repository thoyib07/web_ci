<section id="section-01" class="section-01">
  <div class="section-fixed text-center">
    	<img src="<?php echo base_url().'assets/'; ?>images/nova-29-tahun.png" alt="nova 29 tahun" width="495" height="316" class="animated tada">
    </div>

    <div class="section-01">
    	<div class="container">
        	<h2 class="animated wow fadeIn">#BahagiaItu</h2>
            <div class="picture clearfix">
            	<div class="list col-xs-12 col-sm-6 animated wow fadeInUp">
                    <div class="ellipse">
                        <img src="<?php echo base_url().'assets/'; ?>images/cats.jpg" alt="" width="595" height="595" class="image-01">
                    </div>
                     <div class="desc clearfix">
                        <div class="ellipse-small">
                            <img src="<?php echo base_url().'assets/'; ?>images/raisa.jpg" alt="" width="640" height="400">
                        </div>
                        <p class="text">#bahagiaitu kalau baru nyampe rumah terus disambut sama kucing-kucingku padahal sebenernya mereka laper.
 <span class="name">Raisa (Penyanyi)</span></p>
                    </div>
                </div><div class="list col-xs-12 col-sm-6 animated wow fadeInUp" data-wow-delay="0.2s">
                    <div class="ellipse">
                        <img src="<?php echo base_url().'assets/'; ?>images/eat.jpg" alt="" width="600" height="598" class="image-01">
                    </div>
                    <div class="desc">
                        <div class="ellipse-small">
                            <img src="<?php echo base_url().'assets/'; ?>images/isyana.jpg" alt="" width="512" height="648">
                        </div>
                        <p class="text">#bahagiaitu kalau aku bisa makan apa tapi gak gendut. Apalagi nasi padang, sop buntut, sama daging dan rendang<span class="name">Isyana Sarasvati (Penyanyi)</span></p>
                    </div>
                </div><div class="list col-xs-12 col-sm-6 animated wow fadeInUp" data-wow-delay="0.4s">
                    <div class="ellipse">
                        <img src="<?php echo base_url().'assets/'; ?>images/sleep.jpg" alt="" width="500" height="501" class="image-01">
                    </div>
                    <div class="desc">
                        <div class="ellipse-small">
                            <img src="<?php echo base_url().'assets/'; ?>images/atiqah.jpg" alt="" width="300" height="300">
                        </div>
                        <p class="text">#bahagiaitu jika pekerjaan selesai semua jadi tidurnya enak atau pulas.<span class="name">Atiqah Hasiholan (artis)</span></p>
                    </div>
                </div><div class="list col-xs-12 col-sm-6 animated wow fadeInUp" data-wow-delay="0.6s">
                    <div class="ellipse">
                        <img src="<?php echo base_url().'assets/'; ?>images/present.jpg" alt="" width="600" height="599" class="image-01">
                    </div>
                    <div class="desc">
                        <div class="ellipse-small">
                            <img src="<?php echo base_url().'assets/'; ?>images/ratnawati.jpg" alt="" width="350" height="350">
                        </div>
                        <p class="text">#bahagiaitu pas pengen sesuatu dalam hati, eeh tiba-tiba ada yang ngasih.<span class="name">Ratnawati Sutedjo (Perempuan Inspiratif Nova 2011)</span></p>
                    </div>
                </div><div class="list col-xs-12 col-sm-6 animated wow fadeInUp" data-wow-delay="0.8s">
                    <div class="ellipse">
                        <img src="<?php echo base_url().'assets/'; ?>images/flower.jpg" alt="" width="283" height="213" class="image-01">
                    </div>
                    <div class="desc">
                        <div class="ellipse-small">
                            <img src="<?php echo base_url().'assets/'; ?>images/rita.jpg" alt="" width="300" height="302">
                        </div>
                        <p class="text">#bahagiaitu kalau tanaman/kembang yang aku rawat berbunga/berbuah.<span class="name">Rita Atmaja (Perempuan Inspiratif Nova 2008)</span></p>
                    </div>
                </div>
            </div>
            <div class="quote col-md-offset-2 col-md-8 col-xs-12 animated wow fadeInUp">
            	<p>
                	Kebahagiaan tidak selalu terjadi karena hal yang besar<br>
Sesuatu yang kecil juga bisa dirayakan sebagai kebahagiaan<br><br>
Kalau kebahagiaan kecil versi Anda seperti apa sih?
                </p>
            </div>
            <div class="col-md-12 col-xs-12 text-center animated wow fadeInUp">
            	<a href="#" class="btn-pink">Bagi ceritamu disini!</a>
            </div>
        </div>
    </div>
</section>
