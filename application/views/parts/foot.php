<div class="footer">
    	<div class="container">
            <div class="col-md-2 col-xs-12 footer-box">
              	<p><img src="<?php echo base_url().'assets/'; ?>images/klub-nova.png" alt="" class="img-responsive" width="104" height="33"></p>
                <p>nova.id</p>
            </div>

            <a href="https://www.youtube.com/user/tabloidnovachannel">
              <div class="col-md-3 col-xs-12 footer-box">
              	 <p><img src="<?php echo base_url().'assets/'; ?>images/youtube.png" alt="" class="img-responsive" width="49" height="20"></p>
                 <p>TABLOID NOVA CHANNEL</p>
              </div>
            </a>

            <a href="https://www.instagram.com/tabloidnovaofficial/">
              <div class="col-md-2 col-xs-12 footer-box">
                	<p><img src="<?php echo base_url().'assets/'; ?>images/ig-footer.png" alt="" class="img-responsive" width="20" height="20"></p>
                  <p>@tabloidnovaofficial</p>
              </div>
            </a>

            <a href="https://twitter.com/tabloidnova">
              <div class="col-md-2 col-xs-12 footer-box">
                	<p><img src="<?php echo base_url().'assets/'; ?>images/tw-ico.png" alt="" class="img-responsive" width="30" height="30"></p>
                  <p>@tabloidnova</p>
              </div>
            </a>

            <a href="https://www.facebook.com/wwwtabloidnovacom/">
              <div class="col-md-3 col-xs-12 footer-box">
                	<p><img src="<?php echo base_url().'assets/'; ?>images/fb-ico.png" alt="" class="img-responsive" width="30" height="30"></p>
                  <p>www.tabloidnova.com</p>
              </div>
            </a>

        </div>
    </div>

    <script type="text/javascript">
    $('.datepicker').datepicker({
        color: 'red',
        startDate: '01-01-1960',
        endDate: '31-12-2000',
        format: 'dd-mm-yyyy',
        autoclose: true
    });
  </script>
