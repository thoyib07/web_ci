<section id="section-06" class="section-06">
  <div>
    <div class="container">
      <h2 class="animated wow fadeInDown">#BahagiaItu</h2>
      <div class="img-preview animated wow fadeInDown">
        <img src="<?php echo base_url().'assets/'; ?>images/upload.jpg" alt="" class="img-responsive">
      </div>
      <form action="<?php echo base_url().'home/cerita'; ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 animated wow fadeInDown">
          <div class="col-md-6 col-sm-6 col-xs-7">
            <input type="file" name="gambar" alt="BROWSE" class="img-responsive">
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 size">
            <p>*Gambar Yang Di Upload Max 2 mb</p>
          </div>
        </div>
        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 text-center animated wow fadeInDown">
          <textarea name="cerita" placeholder="Tuliskan cerita Anda diawali #bahagiaitu"></textarea>
          <button type="submit" name="submit" value="submit"><img src="<?php echo base_url().'assets/'; ?>images/btn-submit.png" alt="SUBMIT" class="img-responsive"></button>
        </div>
      </form>
    </div>
  </div>
</section>
