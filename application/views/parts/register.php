<section id="section-04" class="section-04">
  <div class="section-04">
    	<div class="container">
        	<div class="text-center animated wow fadeInUp">
            	<p>Isi data diri Anda dengan lengkap</p>
            </div>
            <form action="<?php echo base_url().'home/regis'; ?>" method="post">
            	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="col-md-6 col-xs-12 animated wow fadeInUp">
                  <input type="text" name="username" placeholder="username" required>
                </div>
                <div class="col-md-6 col-xs-12 animated wow fadeInUp">
                  <input type="password" name="password" placeholder="password" required>
                </div>
              </div>
              <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="col-md-6 col-xs-12 animated wow fadeInUp">
                  <input type="text" name="nama" placeholder="nama lengkap" required>
                </div>
                <div class="col-md-6 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2 radio-btn animated wow fadeInUp">
                  <label class="custom-control custom-radio">
                    <input id="radio1" name="jk" type="radio" class="custom-control-input" value="1" required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Pria</span>
                  </label>
                  <label class="custom-control custom-radio">
                    <input id="radio2" name="jk" type="radio" class="custom-control-input" value="0">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Wanita</span>
                  </label>
                </div>
              </div>
              <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="col-md-6 col-xs-12 animated wow fadeInUp">
                  <input type="text" name="tempat_lahir" placeholder="tempat lahir" required>
                </div>
                <div class="col-md-6 col-xs-12 animated wow fadeInUp">
                  <input class="datepicker" type="text" name="tgl_lahir" placeholder="tanggal lahir (dd/mm/yy)" readonly required>
                </div>
              </div>
              <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 animated wow fadeInUp">
                <textarea placeholder="alamat lengkap" name="alamat" required></textarea>
              </div>
              <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="col-md-6 col-xs-12 animated wow fadeInUp">
                  <input type="email" name="email" placeholder="email" required>
                </div>
                <div class="col-md-6 col-xs-12 animated wow fadeInUp">
                  <input type="text" name="hp" placeholder="No HP" pattern="[0-9]{0,13}" maxlength="13" required>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 text-center animated wow fadeInUp">
                <button type="submit"><img src="<?php echo base_url().'assets/'; ?>images/btn-submit.png" alt="SUBMIT" class="img-responsive"></button>
              </div>
            </form>
        </div>
    </div>
    
  <div class="modal fade" id="myRegis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <div class="detail-img">
            <p>Untuk memverifikasi akun, cukup klik tautan yang dikirim ke email yang anda daftarkan.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
