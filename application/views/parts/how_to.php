<section id="section-02" class="section-02">
  <div>
    	<div class="container mekanisme">
        	<div class="col-md-12 animated wow fadeInLeft"><h2 class="text-center"><img src="<?php echo base_url().'assets/'; ?>images/mekanisme.png" class="img-responsive" alt="Mekanisme" width="373" height="50"></h2></div>
            <div class="col-md-2 col-md-offset-1 box animated wow bounceIn">
            	<img src="<?php echo base_url().'assets/'; ?>images/mekanisme-01.png" alt="1" class="img-responsive">
                <p class="text-center">Beli Tabloid NOVA & cari kode kupon yang ada di edisi 1513 - 1520</p>
            </div>
            <div class="col-md-2 box animated wow bounceIn" data-wow-delay="0.2s">
            	<img src="<?php echo base_url().'assets/'; ?>images/mekanisme-02.png" alt="1" class="img-responsive">
                <p class="text-center">Registrasi diri Anda</p>
            </div>
            <div class="col-md-2 box animated wow bounceIn" data-wow-delay="0.4s">
            	<img src="<?php echo base_url().'assets/'; ?>images/mekanisme-03.png" alt="1" class="img-responsive">
                <p class="text-center">Masukkan 2 kode yang berbeda</p>
            </div>
            <div class="col-md-2 box animated wow bounceIn" data-wow-delay="0.6s">
            	<img src="<?php echo base_url().'assets/'; ?>images/mekanisme-04.png" alt="1" class="img-responsive">
                <p class="text-center">Upload foto dan berikan caption</p>
            </div>
            <div class="col-md-2 box animated wow bounceIn last" data-wow-delay="0.8s">
            	<img src="<?php echo base_url().'assets/'; ?>images/mekanisme-05.png" alt="1" class="img-responsive">
                <p class="text-center">Share ke social media Anda</p>
            </div>

            <div class="col-md-12 animated wow fadeInLeft"><h2 class="text-center"><img src="<?php echo base_url().'assets/'; ?>images/s&k.png" class="img-responsive" alt="Mekanisme" width="374" height="50"></h2></div>
            <div class="col-md-10 col-md-offset-1 sk">
            	<p class="animated wow fadeInLeft">Ketentuan Kontes:</p>
                <ol class="animated wow fadeInLeft">
                	<li>Untuk mengikuti kontes ini Anda harus mendapatkan kode digital yang ada di <span>Tabloid NOVA edisi 1513-1520</span></li>
                    <li>Kode digital terdiri dari <span>4 (kombinasi huruf dan angka)</span></li>
                    <li>Harus memasukkan <span>2 kode yang berbeda,</span> kode yang sudah dimasukkan tidak dapat digunakan kembali</li>
                    <li>Upload foto yang menceritakan kebahagiaan versi Anda dengan hashtag <span>#bahagiaitu</span></li>
                    <li>Ceritakan tentang momen tersebut</li>
                    <li>Cerita dan foto tidak mengandung unsur kekerasan, pelecehan, pornografi, tidak menyinggung SARA dan tidak melanggar hak milik intelektual pihak lain manapun</li>
                    <li>Boleh upload lebih dari 1 foto dengan kupon berbeda</li>
                </ol>
                <p class="animated wow fadeInLeft">Syarat Umum :</p>
                <p class="animated wow fadeInLeft no-dec">Kontes ini terbuka bagi mereka yang memenuhi syarat sebagai berikut:</p>
                <ol class="animated wow fadeInLeft">
                	<li><span>Warga Negara Indonesia (WNI)</span></li>
                    <li>Berdomisili di seluruh wilayah negara <span>Republik Indonesia.</span></li>
                    <li>Program ini berlangsung mulai dari <span>20 Februari – 17 April 2017</span></li>
                    <li>Pemenang Utama akan diumumkan di link <span>microsite, Sosial Media NOVA, dan Tabloid NOVA</span></li>
                    <li>Hadiah akan dikirimkan oleh pihak NOVA 1 bulan setelah pengumuman pemenang.</li>
                    <li>Setiap foto dan cerita yang diikutsertakan dan dikirimkan dalam Program harus merupakan <span>kreasi asli dan milik pribadi Peserta</span></li>
                    <li>Penyelenggara tidak bertanggung jawab atas kerugian maupun kerugian pihak ketiga mana pun yang timbulkan dari hasil karya Peserta.</li>
                    <li>Cerita yang masuk menjadi milik <span>redaksi NOVA</span></li>
                </ol>
            </div>
        </div>
    </div>
</section>
