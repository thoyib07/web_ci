<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'assets/'; ?>images/NOVA.png" alt="NOVA" width="98" height="50"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#section-01">HOME <span class="sr-only">(current)</span></a></li>
        <li><a href="#section-02">ABOUT</a></li>

        <?php if ($this->session->userdata['login'] == 'masuk'): ?>

          <?php if ($this->session->userdata['valid_kode'] == "valid"): ?>
              <li><a href="#section-06">UPLOAD</a></li>
            <?php else: ?>
              <li><a href="#section-05">UPLOAD</a></li>
            <?php endif; ?>

        <?php else: ?>
          <li><a href="#">UPLOAD</a></li>
        <?php endif; ?>


        <li><a href="#section-07">GALLERY</a></li>

        <?php if ($this->session->userdata['login'] == 'masuk'): ?>
          <li><a href="<?php echo base_url().'home/logout' ?>">LOGOUT</a></li>
        <?php else: ?>
          <li><a href="#section-03">LOGIN</a></li>
        <?php endif; ?>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
