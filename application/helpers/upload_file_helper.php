<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function unggah_berkas($path, $nama_file, $type) {

	$CI = get_instance();
	$config['upload_path'] = './assets/uploads/'.$path.'/';
	$config['allowed_types'] = $type;
	$config['max_size']	= '262144';	
	$config['file_name'] = $nama_file;
	$config['overwrite'] = TRUE;

	$CI->upload->initialize($config);

	return $config['file_name'];
}

/* End of file upload_file_helper.php */