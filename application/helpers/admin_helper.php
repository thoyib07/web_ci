<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function random($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function email($value) {
	$data['email']			= $value['email'];
	$data['token']			= $value['token'];
	$data['name'] 			= $value['user'];
	$data['subject'] 		= 'Lupa Password SISTAD';
	$data['message'] 		= 'Klik link dibawah ini untuk mereset password anda. <br> '.base_url().'admin/forget/'.$value['token'];
	$data['created_date'] 	= $value['date'];

	$CI = get_instance();
    $CI->load->model('m_admin');
    $CI->m_admin->addMail($data);

	$CI->email->from('thoyib07@gmail.com'); //replace to in email configuration file
	$CI->email->to($data['email']);
	$CI->email->reply_to('thoyib07@gmail.com'); //replace to in email configuration file
	$CI->email->subject($data['subject']);
	$CI->email->message($data['message']);
	var_dump($CI->email->item('smtp_user','email'));die();
	//$CI->email->send();

	redirect(base_url().'admin/');
}

function active_time() {
	$timer = 60 * 60; //60 menit
	$CI = get_instance();
	// var_dump($_SESSION);
	// die();
	if ($CI->session->userdata('login_admin') == 1) {
		update_privilage();
	}

	if ($CI->session->userdata('login_admin') != 1) {
		redirect('auth/login');
	} elseif (time() - $CI->session->userdata('time_admin') > $timer) {
		$session = array('login_admin' => FALSE, 'time_admin' => '');
		// $session = array('login' => true, 'time' => time(), 'id_user' => '1');
		$CI->session->set_userdata($session);
		redirect('admin/index');
	}
}

function update_privilage() {
	$CI = get_instance();
	$data_user['select'] 	= "*";
	$data_user['table']		= "m_user";
	$data_user['where']		= "id_user = '".$_SESSION['id_user_admin']."' and status = '1'";
	$data['user'] = $CI->m_admin->getData($data_user);
	// var_dump($_SESSION);
	// die();
	// $data_user_akses['select'] 		= "*";
	// $data_user_akses['table']		= "user_akses";
	// $data_user_akses['where']		= "user_id = '".$data['user']['0']['id_user']."' and status = '1'";
	// $data['user_akses_all'] 		= $CI->m_admin->getData($data_user_akses);
	$session = array('id_user_admin' => $data['user']['0']["id_user"], 'akses_admin' => $data['user']['0']['id_user_lvl']);
	$CI->session->set_userdata($session);
}

function info_user() {
	$CI = get_instance();
	$user['select']		= "u.id_user,u.nama,l.role";
	$user['table']		= "m_user as u";
	$user['join'][0]	= array('m_user_lvl as l', 'l.id_user_lvl = u.id_user_lvl' );
	$user['where'] 		= "u.status = 1 and u.id_user = '".$CI->session->userdata('id_user_admin')."'";
	$user['limit']		= "limit 1";
	$data['user'] 		= $CI->m_admin->getData($user);

	return $data['user'];
}

function list_table()
{	 
	$CI = get_instance();
	$tables=$CI->db->query("SHOW TABLES FROM `sistad` LIKE 'd_%'")->result_array();    
	var_dump($tables);
 	/*
 	foreach($tables as $key => $val) {
      echo $val['myTables']."<br>";// myTables is the alias used in query.
 	}
 	*/
}

function isSuper() {
	$CI = get_instance();
	if ($CI->session->userdata('akses_admin') == "1") {
		return TRUE;
	} else {
		return FALSE;
	}
}

function isOperator() {
	$CI = get_instance();
	if ($CI->session->userdata('akses_admin') == "2") {
		return TRUE;
	} else {
		return FALSE;
	}
}

/* End of file upload_file_helper.php */